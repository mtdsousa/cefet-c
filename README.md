Linguagem C
==================
Sobre o Curso
-------------------
Curso ministrado aos estudantes do primeiro período de Engenharia de Computação do Centro Federal de Educação Tecnológica (CEFET) de Minas Gerais.

  * **Professor**: Bruno A. Santos
  * **Referência**: DAMAS, Luís. Linguagem C
  * **Módulos**
    * I - O Meu Primeiro Programa
    * II - Tipos de Dados Básicos
    * III - Testes e Condições
    * IV - Laços
    * V - Funções e Procedimentos
    * VI - Vetores
    * VII - Strings
    * VIII - Ponteiros
    * IX - Passagem de Parãmetros
    * X - Arquivos
    * XI - Estruturas, Memória dinâmica e Listas
  * **Avaliações**
    * I - Módulo I ao V
    * II - Módulos VI, VII e VIII
  * **Extras**
    * Jogo da Velha (velha.c)

*Notações*: `e01.c` para exercício 01 do livro texto. `ec01.c` para exercício complementar 01.
