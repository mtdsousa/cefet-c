/*

Considere as seguintes informações:
1. Dia 1º de Janeiro de 1900 foi segunda-feira.
2. Abril, Junho, Setembro e Novembro possuem 30 dias.
3. Todos os outros meses têm 31 dias, exceto o mês de fevereiro que tem 28 dias.
    -> Quando o ano é bissexto, o mês de fevereiro tem 29 dias.
4. Um ano é bissexto quando:
    -> O ano não é divisível por 100 e é divisível por 4, ou
    -> O ano é divisível por 100 e por 400.

Implemente um programa que conta quantos domingos caíram no primeiro
dia do mês durante o século 20 (1º de Janeiro de 1901 até 31 de Dezembro de 2000).

Resposta: 171

*/

#include <stdio.h>

int bissexto(int ano){
    if((ano%100!=0)&&(ano%4==0)){
        return 1;
    }
    if((ano%100==0)&&(ano%400==0)){
        return 1;
    }
    return 0;
}

void printa(int *a, int len){
    int i;
    for(i=0; i<len; i++){   
        printf("%d ", a[i]);
    }
}

/*
0: Janeiro
1: Fevereiro
2: Março
3: Abril
4: Maio
5: Junho
6: Julho
7: Agosto
8: Setembro
9: Outubro
10: Novembro
11: Dezembro
*/
int diasdomes(int mes, int ano){
    switch(mes){
        case 1:
            if(bissexto(ano)){
                return 29;          
            }else{
                return 28;          
            }
            break;
        case 3:
        case 5:
        case 8:
        case 10:
            return 30;
            break;
        default:
            return 31; 
    }
}

/*
0: Domingo
1: Segunda
2: Terça
3: Quarta
4: Quinta
5: Sexta
6: Sábado
*/
int main(){ 
    int d, m, a;
    int dia_semana = 1;
    int domingos = 0;
    for(a=1900; a<=2000; a++){
        for(m=0; m<12; m++){
            for(d=0; d<diasdomes(m,a); d++){                
                if(dia_semana>6){
                    dia_semana = 0;     
                }
                if(dia_semana==0 && d==0){
                    if(a!=1900){
                        domingos++;     
                    }
                }   
                dia_semana++;
            }
        }
    }   
    
    printf("Resultado:\nDomingos: %d\n\n", domingos);   
    return 0;
}
