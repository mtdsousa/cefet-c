/*

Implemente um programa que preenche dois vetores Q e R com 8
e 10 elementos inteiros (valores repetidos ou não) respectivamente. Implemente um
procedimento que recebe os vetores Q, R e um terceiro vetor W. O procedimento
deverá preencher o vetor W com a interseção de Q e R. Além disto, W não poderá
conter elementos repetidos. Exiba o conteúdo do vetor W no programa principal.

Teste seu programa com os valores: Q={1,2,3,3,2,6,4,5} e R={5,7,8,9,2,2,10,5,2,6}
Saída: 2, 5, 6 (Não necessariamente nesta ordem).

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void printa(int *a, int len){
    int i;
    for(i=0; i<len; i++){
        printf("%d ", a[i]);    
    }
    putchar('\n');
}

int existe(int *a, int len, int n){
    int i;
    for(i=0; i<len; i++){
        if(a[i]==n){
            return 1;
        }
    }
    return 0;
}

int intersecao(int *q, int *r, int *w){
    int i;
    int f = 0;
    for(i=0; i<10; i++){
        if((existe(q, 8, r[i]))&&(!existe(w, f, r[i]))){    
            w[f++] = r[i];      
        }
    }
    return f;
}

int main(){ 
    int i, j;
    int q[8];
    int r[10];
    int w[10];
    int ir = 0;
    printf("Entre:\nA) Com q:\n");
    for(i=0; i<8; i++){
        printf("%d. ", i+1);
        scanf("%d", &q[i]);
    }
    printf("\nB) Com r:\n");
    for(i=0; i<10; i++){
        printf("%d. ", i+1);
        scanf("%d. ", &r[i]);   
    }
    ir = intersecao(q, r, w);
    printa(w, ir);
    return 0;
}
