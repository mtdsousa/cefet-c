/*

Qual número inteiro entre 1 e 10000 possui a maior quantidade de divisores? Quantos divisores
este número possui? Escreva um programa para calcular e exibir a resposta. Como podem existir
mais de um número com a mesma quantidade de divisores, seu programa deve exibir apenas o
menor e o maior dos números. Exiba os divisores destes números. 

Resposta: menor número 7560, maior número 9240. Ambos com 64 divisores.

*/

#include <stdio.h>

int d(int n){
    int i, div=0;
    for(i=1; i<=n; i++){
        if(n%i==0){
            div++;
        }
    }
    return div;
}

int main(){
    int i, div=-1, max=-1, min=-1;
    for(i=1; i<10000; i++){
        if(div==-1 || d(i)>=div){
            if(div==-1 || d(i)>div){
                min = i;
                max = i;
            }else if(i<min){
                min = i;
            }else if(i>max){ 
                max = i;
            }
            div = d(i);    
        }
    }
    printf("Resultado: \nMaior: %d. Menor: %d. Ambos com %d divisores. ", max, min, div);
    return 0;
}
