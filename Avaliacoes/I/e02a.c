/*

Uma sequência iterativa é definida por um conjunto de números inteiros positivos. A regra para
geração dos números da sequência é: 

n -> n/2 (quando n é par)
n -> 3n+1 (quando n é ímpar)

Iniciando com n=13, por exemplo, a sequência gerada é:
13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1

Esta sequência inicia-se em 13, termina em 1 e contém 10 termos. Embora ainda não tenha sido
provado, acredita-se que ao iniciar a sequência com qualquer número inteiro positivo, ela sempre
terminará em 1.

Qual o número inicial, menor que 1 milhão, que gera a sequência mais longa, ou seja, com maior
número de termos? Implemente um programa para resolver este problema.

Observação: Após o início da sequência, os termos podem ser maiores que 1 milhão.
Resposta: 837799

*/

#include <stdio.h>

int main(){
    int i = 0;
    int maior, nmaior;
    for (i=1; i<1000000; i++){
        if (i==1){
            maior = sequence(i);
            nmaior = i;
        }else{
            if (sequence(i)>maior){
                maior = sequence(i);
                nmaior = i;
            }
        }
    }
    printf("Resultado: %d \n\n", nmaior);
    return 0;
}

int sequence(unsigned int n){
    int q = 0;
    while(n != 1){
        if (n%2==0){
            n /= 2;
        }else{
            n = (3*n)+1;
        }
        q++;
    }
    q++;
    return q;   
}
