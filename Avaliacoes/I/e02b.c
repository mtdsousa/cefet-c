/*

Considere d(n) a soma dos divisores próprios de n (soma dos divisores de n cujo resto é zero).
Por exemplo, os divisores de 220 são: 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 e 110. A soma destes divisores é
284, ou seja d(220)=284.

Se d(a)=b e d(b)=a, onde a!=b, então a e b são números amigáveis. Por exemplo:
d(220)=284 e d(284)=220. Os números 220 e 284 são números amigáveis. 

Faça um programa para exibir todos os pares de números amigáveis para n<10000.

Saída do programa:
220 284
1184 1210
2620 2924
5020 5564
6232 6368

*/

#include <stdio.h>

int d(int n);

int main(){
	int i; int j;
	for(i=1; i<10000; i++){
		for(j=i+1; j<10000; j++){
			if((d(i) == j) && (d(j)==i) && (i!=j)){
				printf("%d %d\n", i, j);
			}
		}
	}
	return 0;
}

int d(int n){
	int soma = 0, i;
	for(i=1; i<n; i++){
		if(n % i==0)
			soma += i;	
	}
	return soma;
}




