/*

O quadrado da soma dos 10 primeiros números naturais é:
(1+2+3+ ... +10)² = 3025

A soma dos quadrados dos 10 primeiros números naturais é:
1²+2²+3²+ ... +10² = 385

A diferença entre o quadrado da soma e a soma dos quadrados é 3025-385 = 2640.
Implemente uma função que calcula e retorna a diferença entre o quadrado da soma e a
soma dos quadrados dos N primeiros números naturais. A variável N será passada como
parâmetro para função. Implemente o programa principal para testar sua função. Exiba o
retorno da função no programa principal.

Resposta: 25164150 para N=100; 401323300 para N=200.

*/

#include <stdio.h>
#include <math.h>

int main(){
    int n;
    printf("Entre:\n1. N: ");
    scanf("%d", &n);
    printf("Resultado: %d\n\n", qsmsq(n));
    return 0;
}

int qsmsq(int n){
    int i = 0;
    int qs = 0;
    int sq = 0;
    int result; 

    for (i=1; i<=n; i++){
        qs += i;
    }
    qs = pow(qs, 2);
    
    for(i=1; i<=n; i++){
        sq += pow(i, 2);        
    }
    
    result = qs-sq;
    return result;
}
