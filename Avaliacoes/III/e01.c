/*

Implemente um programa para manipular notas de alunos em disciplinas. Crie
uma estrutura contendo as variáveis: "número de matrícula", "nota" e "código
da disciplina".

1. Crie uma lista dinâmica para armazenar os dados desta estrutura. Implemente
um procedimento para inserir os elementos na lista.

2. Crie um procedimento para exibir todos os elementos da lista.

3. Implemente um procedimento que recebe como parâmetro o código da 
disciplina e exibe todos os alunos com as respectivas notas na disciplina
informada.

4. Implemente um procedimento que recebe um número de matrícula como 
parâmetro e remove da lista o aluno correspondente.

5. Implemente um procedimento para gravar os dados da lista em um arquivo
binário. Toda vez que o programa for iniciado, você deve carregar os dados do
arquivo na lista dinâmica. O arquivo binário será utilizado apenas quando o
programa iniciar (para carregar os dados do arquivo na lista) e terminar (para
gravar os dados da lista no arquivo).

6. Implemente uma função (ou procedimento) denominada "eliminar_repetidos"
que elimina matrículas duplicadas, ou seja, o procedimento elimina da lista
ocorrências duplicadas de um único aluno matriculado mais de uma vez na
mesma disciplina.

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define DATA "files/alunos.dat"

struct ALUNO{
    int matricula;
    float nota;
    int cod;

    struct ALUNO * prox;
};
typedef struct ALUNO Aluno;

void save(Aluno * lista){
    remove(DATA);
    Aluno * last = lista;
    if(last==NULL){
        return;
    }
    FILE * data = fopen(DATA, "ab");
    if(data==NULL){
        printf("Erro ao operar arquivos. \n");
        return;
    }
    while(last->prox!=NULL){
        fwrite(last, sizeof(Aluno), 1, data);
        last = last->prox;
    }
    fwrite(last, sizeof(Aluno), 1, data);
}

Aluno * insere(Aluno *lista, int matricula, float nota, int cod){
    Aluno * last = lista;
    Aluno * novo = (Aluno*)malloc(sizeof(Aluno));
    novo->matricula = matricula;
    novo->nota = nota;
    novo->cod = cod;
    novo->prox = NULL;
    if(lista==NULL){
        lista = novo;
        return lista;
    }
    while(last->prox!=NULL){
        last = last->prox;
    }
    last->prox = novo;
    return lista;
}

Aluno * load(Aluno * lista){
    FILE * data = fopen(DATA, "rb");
    Aluno *getf = (Aluno*)malloc(sizeof(Aluno));
    if(data==NULL){
        printf("Erro: Arquivo não encontrado ou em local sem permissão de leitura. Busque por 'chmod'. \n");
        return NULL;
    }
    while(fread(getf, sizeof(Aluno), 1, data)==1){
        lista = insere(lista, getf->matricula, getf->nota, getf->cod);
    }
    return lista;
}

void listar(Aluno *lista){
    Aluno * last = lista;
    if(last==NULL){
        printf("Erro: Lista vazia. \n");
        return;
    }
    while(last->prox!=NULL){
        printf("Aluno %d: Nota %.2f (disciplina %d)\n", last->matricula, last->nota, last->cod);
        last = last->prox;
    }
    printf("Aluno %d: Nota %.2f (disciplina %d)\n", last->matricula, last->nota, last->cod);
}

void busca_disciplina(Aluno * lista, int disciplina){
    int find = 0;
    Aluno * last = lista;
    if(lista==NULL){
        printf("Erro: Não existe alunos. Nem nesta nem em outras disciplinas.\n");
        return;
    }
    while(last->prox!= NULL){
        if(last->cod==disciplina){
            if(!find){printf("Estão na disciplina %d:\n", disciplina);}
            printf("Aluno %d \n", last->matricula);
            find = 1;
        }
        last = last->prox;
    }
    if(last->cod==disciplina){
        if(!find){printf("Está na disciplina %d:\n", disciplina);}
        printf("Aluno %d \n", last->matricula);
        find = 1;
    }
    if(!find){
        printf("Não há alunos na disciplina %d\n", disciplina);
    }
}

int existe(Aluno * lista, int aluno){
    Aluno * last = lista;
    if(lista==NULL){
        return 0;
    }
    while(last->prox!=NULL){
        if(last->matricula == aluno){
            return 1;
        }
        last = last->prox;
    }
    if(last->matricula == aluno){
        return 1;
    }
    return 0;
}

int existe_(Aluno * lista, int aluno, int disciplina){
    Aluno * last = lista;
    int c = 0;
    if(lista==NULL){
        return 0;
    }
    while(last->prox!=NULL){
        if(last->matricula == aluno && last->cod == disciplina){
            c++;
        }
        last = last->prox;
    }
    if(last->matricula == aluno && last->cod == disciplina){
        c++;
    }
    return c;
}

Aluno * excluir(Aluno * lista, int aluno){
    Aluno * last = lista;
    Aluno * aux = NULL;
    if(lista==NULL){
        return;
    }
    while(existe(lista, aluno)){
        if(last->matricula==aluno){
            if(last->prox!=NULL){
                lista = last->prox;
            }else{
                lista = NULL;
            }
            free(last);
            continue;
        }
        while(last->prox!=NULL){
            if(last->prox->matricula == aluno){
                aux = last->prox;
                last->prox = last->prox->prox;
                free(aux);
                continue;
            }
            last = last->prox;
        }
    }
    return lista;
}

Aluno * excluir_(Aluno * lista, int aluno, int disciplina){
    Aluno * last = lista;
    Aluno * aux = NULL;
    if(lista==NULL){
        return;
    }
    if(last->matricula==aluno && last->cod == disciplina){
        if(last->prox!=NULL){
            lista = last->prox;
        }else{
            lista = NULL;
        }
        free(last);
        return lista;
    }
    while(last->prox!=NULL){
        if(last->prox->matricula == aluno && last->prox->cod == disciplina){
            aux = last->prox;
            last->prox = last->prox->prox;
            free(aux);
            return lista;
        }
        last = last->prox;
    }
    return lista;
}

Aluno * eliminar_repetidos(Aluno * lista){
    Aluno * last = lista;
    int matricula;
    int cod;
    if(lista==NULL){
        return;
    }
    while(last->prox!=NULL){
        matricula = last->matricula;
        cod = last->cod;
        while(existe_(lista, matricula, cod)>1){
            lista = excluir_(lista, matricula, cod);
        }
        last = last->prox;
    }
    matricula = last->matricula;
    cod = last->cod;
    while(existe_(lista, matricula, cod)>1){
        lista = excluir_(lista, matricula, cod);
    }
    return lista;
}

int main(){
    Aluno * lista = NULL;
    lista = load(lista);
    listar(lista);
    getchar();

    lista = insere(lista, 1, 6, 1); //listar(lista);
    lista = insere(lista, 2, 7, 2); //listar(lista);
    lista = insere(lista, 3, 8, 1); //listar(lista);
    lista = insere(lista, 4, 9, 3); //listar(lista);
    lista = insere(lista, 5, 10, 1); //listar(lista);
    lista = insere(lista, 2, 11, 2); //listar(lista);
    lista = insere(lista, 4, 12, 2); listar(lista);

    printf("\n\nBuscando alunos da disciplina 1: \n\n");
    busca_disciplina(lista, 1);

    printf("\n\nEliminando repetidos: \n\n");
    lista = eliminar_repetidos(lista); listar(lista);

    printf("\n\nExcluindo aluno 4: \n\n");
    lista = excluir(lista, 4); listar(lista);

    printf("\n\nExcluindo aluno 1: \n\n");
    lista = excluir(lista, 1); listar(lista);

    save(lista);
    return 0;
}
