/*

Escreva um programa que coloque na tela a seguinte saída:

    Total	=	100%
    IVA		=	17%
    IRS		=	15%
    ------------------------
    Liq.	=	68%

*/

#include <stdio.h>
int main(){
    printf("Total\t=\t100%%\n");
    printf("IVA\t=\t17%%\n");
    printf("IRS\t=\t15%%\n");
    printf("--------------------\n");
    printf("Liq\t=\t68%%\n");
    return 0;
}
