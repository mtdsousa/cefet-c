/*

Faça um programa para calcular o valor de S, dado por:
S= 1/1 + 3/2 + 5/3 + 7/4 + ... + 99/50

*/

#include <stdio.h>
int main(){
	int i;
	float soma = 1;
	for (i=2; i<= 50; i++){
		soma += ((2*i)-1)/i;
	}
	printf("Resultado: \n1. Soma: %.2f\n\n", soma);
	return 0;
}
