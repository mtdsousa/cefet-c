/*

Escreva um programa que solicita ao usuário um valor entre 3 e 18. Este
valor representa a soma dos valores do lançamento de 3 dados. Em seguida, o
programa deve imprimir todas as possibilidades de que no lançamento dos 3
dados a soma de seus valores seja igual ao valor informado pelo usuário. Por
exemplo, caso o usuário digite o valor 10, o programa deverá exibir: 6,3,1;
1,6,3; 6,2,2; 5,3,2 etc. Você pode exibir sequências iguais em dados diferentes,
por exemplo, as sequências 6,3,1 e 1,6,3 possuem os mesmos números, mas
em dados diferentes

*/

#include <stdio.h>
int main(){
	int a, i, j, k;
	printf("Entre: \n1. Com valor [3,18]: ");
	scanf("%d", &a);
	printf("Resposta: \n");
	for (i=1; i<=6; i++){
		for (j=1; j<=6; j++){
			for (k=1; k<=6; k++){
				if (i+j+k == a){
					printf("= %d e %d e %d\n",i, j, k);
				}
			}
		}
	}
	return 0;
}
