/*

Dado o valor de E, calcular S=1 + 1/2 + 1/4 +1/6 + ... até que um termo
da série seja menor do que E. O valor de E deverá ser digitado pelo usuário

*/

#include <stdio.h>
int main(){
	float e;
	int i = 0;
	int num = 0;
	float termo = 1;
	float soma = 0;

	printf("Entre: \n1. Valor de E: ");
	scanf("%f", &e);
	if (termo < e){
		printf("Resultado: \n Não existiu termos maiores ou iguais que %.2f.\n\n", e);
		return 0;
	}
	while(termo >= e){
		soma += termo;
		i+=2;
		termo = (1/(float)i);
		num++;
	}
	printf("Resultado: \n1. Existiu %d termos.\n2. A soma deles é igual a: %.3f\n\n", num, soma);
	return 0;
}
