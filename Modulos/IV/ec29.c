/*

Faça um programa que solicita ao usuário dois números inteiros
diferentes "n1" e "n2" e calcula a soma de todos os números ímpares dentro do
intervalo definidor por [n1,...,n2]. Considere que n1 é sempre menor do que n2

*/

#include <stdio.h>
int main(){
	int a, b, soma, i, tmp;
	printf("Entre:\n1. N1: ");
	scanf("%d", &a);
	do{
		printf("Entre: \n2. N2 para N2 <> N1: ");
		scanf("%d", &b);
		if (a==b){
			printf("Erro: Valor inválido. Tente novamente. \n\n");
		}
	}while(a==b);
	if (a>b){
		tmp = a;
		a = b;
		b = tmp;
	}
	i = a+1;
	soma = 0;
	for (i; i<b; i++){
		if (i%2!=0){
			soma += i;
		}
	}
	printf("Resultado:\n1. Soma dos ímpares [%d,...,%d]: %d\n\n", a, b, soma);
	return 0;
}
