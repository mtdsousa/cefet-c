/*

Otimize o programa anterior (ec31.c) com base nas seguintes considerações:

* Números pares (com exceção do 2) não podem ser primos, visto que são
divisíveis por 2. Se um número não for divisível por 2, não será divisível por
nenhum outro número par. Portanto, com excecão do número 2, só precisaremos 
testar números ímpares.

* É mais fácil que um número seja divisível por um número pequeno do que por
um número maior. Portanto, se iniciarmos a procura do divisor de baixo para
cima, ao invés de cima para baixo, como foi implementado, teremos chance de
encontrar o número muito antes.

* Nenhum número pode ser divisível por outro número maior que a metade dele.
Portanto, não precisamos testar a divisibilidade dos números na faixa entre a
metade e o próprio número.

*/

#include <stdio.h>
int main(){
	int num, i;
	int teste = 0;
	printf("Entre: \n1.Entre com um número: ");
	scanf("%d", &num);
	if (num%2==0 && num != 2){
		printf("Resposta: \n1. %d não é um número primo. :D \n\n", num);
		return 0;
	}
	for (i=1; i<=num; i++){
		if (i>(num/2)){
			break;
		}
		if (num%i==0){
			teste++;
			if (teste>2){
				printf("Resposta: \n1. %d não é um número primo. \n\n", num);
				return 0;
			}
		}
	}
	printf("Resposta: \n1. %d é um número primo.\n\n", num);
	return 0;
}

