/*

Faça um programa para calcular o valor de S, dado por:
S= 1/1 – 2/4 + 3/9 – 4/16 + ... - 10/100

*/

#include <stdio.h>
int main(){
	int i=0;
	float soma =1;
	for (i=2; i<=10; i++){
		if(i%2==0){
			soma -= (float)i/(float)(i*i);
		}else{
			soma += (float)i/(float)(i*i);
		}
	}
	printf("Resultado:\n1. Soma: %f.\n\n", soma);
	return 0;
}
