/*

Faça um programa que leia 10 números digitados pelo usuário e retorne a
soma e a média desses valores

*/

#include <stdio.h>
int main(){
	int i;
	int soma = 0;
	float media =0;
	int temp = 0;
	printf("Entre: \nCom 10 números em sequência:\n");
	for (i=0; i<10; i++){
		printf("%d. ", i+1);
		scanf("%d", &temp);
		soma += temp;
	}
	media = soma/10;
	printf("Resultado: \n1. A soma é: %d\n2. A média é: %.2f\n\n", soma, media);
	return 0;
}
