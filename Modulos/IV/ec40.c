/*

O número 2520 é divisível (resto zero) por todos números inteiros de 1 a
10. Faça um programa, em linguagem C, que encontre o menor número inteiro
positivo divisível por todos os inteiros de 1 a 20.
Resposta: 232792560

*/

#include <stdio.h>
int main(){
	int i, found, num = 20;
	do{
		found = 1;
		num += 20;
		for (i=1; i<=20; i++){
			if (num%i!=0){
				found = 0;
			}
		}
	}while(!found);
	printf("Resultado:\n%d\n\n", num);
}
