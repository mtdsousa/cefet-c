/*

Os números naturais menores do que 10 e múltiplos de 3 ou 5 são: 3, 5, 6
e 9. A soma destes múltiplos é 23. Faça um programa que encontre a soma de
todos os múltiplos de 3 ou 5 menores do que 1000.
Resposta: 233168

*/

#include <stdio.h>
int main(){
	int i, soma = 0;
	for (i=1; i<1000; i++){
		if ((i%3==0)||(i%5==0)){soma += i;}
	}
	printf("Resultado: \n1. Soma de múltiplos de 3 e 5 até 1000: %d.\n\n", soma);
	return 0;
}
