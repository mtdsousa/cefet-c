/*

Faça um programa para calcular e mostrar o valor de PI, usando a série:
PI=4- 4/3 + 4/5 – 4/7 + 4/9 -... até que um termo seja menor do que 0.0001,
em valor absoluto. Use a função “abs(numero)” para determinar o valor
absoluto de um número

*/

#include <stdio.h>
#include <stdlib.h>

int main(){
	float pi, termo;
	int i;
	pi = 0;
	termo = 0;
	i = 0;
	do{
		if (i==0){termo = 4;}
		if (i%2 == 0){pi += termo;}
		else{pi -= termo;}

		i++;
		termo = 4/(float)((i*2) +1);

	}while(termo >= 0.0001);
	printf("Resultado:\n1. Valor de PI: %f\n2. Valor absoluto de PI: %d.\n\n", pi, abs(pi));
	return 0;
}
