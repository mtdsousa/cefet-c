/*

Faça um programa que leia um número inteiro positivo e determine se
este é primo ou não. Por definição, um número é primo quando é divisível
somente por si próprio e por 1

*/

#include <stdio.h>
int main(){
	int num, i;
	int teste = 0;
	printf("Entre: \n1.Entre com um número: ");
	scanf("%d", &num);
	for (i=num; i>=1; i--){
		if (num%i==0){
			teste++;
		}
	}
	if (teste >2){
		printf("Resposta: \n1. %d não é um número primo. \n\n", num);
	}else{
		printf("Resposta: \n1. %d é um número primo.\n\n", num);
	}
	return 0;
}
