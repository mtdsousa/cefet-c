/*

Faça um programa usando o comando "for" para calcular o seguinte
somatório:
n
∑ (5*i+2)
i=3
em que "n" é definido pelo usuário

*/

#include <stdio.h>

int main(){
	int i, n, res = 0;
	printf("Entre:\n1. Com N: ");
	scanf("%d", &n);
	for (i=3; i<= n; i++){
		res += 5*i+2;
	}
	printf("Resultado: \n1. O valor do somatório: %d.\n\n", res);
	return 0;
}
