/*

Adapte o programa anterior para obrigar o usuário a entrar com um valor
para "n1" menor que o valor definido "n2". Para isso, faça um laço que garanta
a entrada de um intervalo válido

*/

#include <stdio.h>
int main(){
	int a, b, soma, i, tmp;
	printf("Entre:\n1. N1: ");
	scanf("%d", &a);
	do{
		printf("Entre: \n2. N2 para N2 > N1: ");
		scanf("%d", &b);
		if (a>=b){
			printf("Erro: Valor inválido. Tente novamente. \n\n");
		}
	}while(a>=b);
	i = a+1;
	soma = 0;
	for (i; i<b; i++){
		if (i%2!=0){
			soma += i;
		}
	}
	printf("Resultado:\n1. Soma dos ímpares [%d,..,%d]: %d\n\n", a, b, soma);
	return 0;
}
