/*

Faça um programa que calcule o fatorial de um número. Se o número for
menor do que zero, então o algoritmo deverá informar ao usuário que o valor é
inválido

*/

#include <stdio.h>
int main(){
	int fator = 0;
	int fatorial = 0;
	int i=0;
	do{
		printf("Entre: \n1. Entre com um número: ");
		scanf("%d", &fator);
		if (fator < 0){
			printf("Erro: Valor Inválido. Tente novamente. \n\n");
		}
	}while(fator <0);
	for (i = 1; i<= fator; i++){
		if (i == 1){
			fatorial = 1;
		}
		fatorial *= i;
	}
	printf("Resultado: \n1. Fatorial de %d: %d.\n\n", fator, fatorial);
	return 0;
}
