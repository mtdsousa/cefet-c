/*

Altere o programa anterior (ec25.c), de tal maneira que o usuário informe a
quantidade de números que serão digitados (ou seja, o valor “10” não deve ser
fixo no programa)

*/

#include <stdio.h>
int main(){
	int i;
	int soma = 0;
	float media =0;
	int temp = 0;
	int execucao = 0;
	do{
		printf("Entre: \n1. Número de repetições: \n");
		scanf("%d", &execucao);
	}while(execucao<=0);
	printf("Entre: \n2. Com %d números em sequência:\n", execucao);
	for (i=0; i<execucao; i++){
		printf("%d. ", i+1);
		scanf("%d", &temp);
		soma += temp;
	}
	media = soma/execucao;
	printf("Resultado: \n1. A soma é: %d\n2. A média é: %.2f\n\n", soma, media);
	return 0;
}


