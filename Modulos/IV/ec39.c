/*

Cada novo termo da sequência de Fibonacci é gerado pela adição dos 2
termos anteriores. Ao iniciar a sequência com 1 e 2, os dez primeiros termos
são: 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ... Faça um programa que encontre a soma
dos números pares da sequência de Fibonacci cujo termo não exceda 4 milhões.
Resposta: 4613732

*/

#include <stdio.h>
int main(){
	int a, b, i,soma=0;
	a = 1;
	b = 2;
	i = 0;
	printf ("1 2 ");
	do{
		if (i%2==0){
			a += b;
			if (b<4000000 && b%2==0) soma += b;
			printf("%d ", a);
		}else{
			b += a;
			if((a<4000000) && (a%2==0)) soma += a;
			printf("%d ", b);
		}
		i++;
	}while(a<4000000 && b<4000000);
	printf("\n\n %d\n",soma);
	return 0;
}
