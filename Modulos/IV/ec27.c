/*

Faça um programa que calcule a multiplicação de 2 números inteiros sem
utilizar o operador "*". Em vez disso, utilize apenas o operador de adição "+"

*/

#include <stdio.h>
int main(){
	int a = 0;
	int b = 0;
	int i = 0;
	int resultado = 0;
	printf("Entre: \nSendo (AxB)\n1. Valor de A: ");
	scanf("%d", &a);
	printf("2. Valor de B: ");
	scanf("%d", &b);
	for (i=0; i<a; i++){
		resultado += b;
	}
	printf("Resultado: \n1. %d x %d = %d.\n\n", a, b, resultado);
	return 0;
}


