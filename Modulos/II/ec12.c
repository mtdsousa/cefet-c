/*

Faça um programa que leia um número inteiro de 4 dígitos e escreva-o
invertido. Por exemplo, se o número lido for 2548, o resultado será 8452. Dica:
utilize o comando “%” que retorna resto da divisão entre 2 números inteiros.

*/

#include <stdio.h>
int main(){
    int numero = 0;
    printf("Entre: \n1. Número de 4 dígitos: ");
    scanf("%d",&numero);
    printf("Resultado: \na) Número invertido %d%d%d%d\n", (((numero%1000)%100)%10), (int)(((numero%1000)%100)/10), (int)((numero%1000)/100), (int)numero/1000);
    return 0;
}
