/*

Faça um programa que calcula a média de quatro números introduzidos
pelo usuário.

*/

#include <stdio.h>
int main(){
    float leitura, soma = 0;
    int i;
    printf("Entre: \n");
    for (i = 0; i<4; i++){
	printf("%d. ", i+1);    
        scanf("%f", &leitura);
        soma += leitura;
    }
    soma /= 4;
    printf("Resultado: \na) Média: %.2f\n", soma);
    return 0;
}
