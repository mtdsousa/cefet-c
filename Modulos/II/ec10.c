/*

Escreva um programa para determinar a quantidade de litros de
combustível gastos em uma viagem por um automóvel que faz 12 km/litro. Para
isso, sabe-se que o tempo gasto na viagem é T=35 min e a velocidade média
do automóvel é V = 80 km/h.

Litros Gastos por quilômetro rodado: f(x) = Xx0.0833
Minutos para Hora: g(x) = Xx0,016666667

*/

#include <stdio.h>
int main(){
    float litros = ((35*0.016666667)*80)*0.0833;
    printf("Resultado: \n a) Litros: %f\n", litros);
    return 0;
}
