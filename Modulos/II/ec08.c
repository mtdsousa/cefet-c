/*

Faça um programa que recebe dois valores na variáveis A e B e, em seguida,
troca o conteúdo destas variáveis. Por exemplo, se o usuário digitar A=5 e B=3,
o programa deverá trocar os valores de tal maneira que A seja igual a 3 e B
igual a 5.

*/

#include <stdio.h>
int main(){
    int a, b, temp;
    printf("Entre: \n1. Valor de A: ");
    scanf("%d", &a);
    printf("2. Valor de B: ");
    scanf("%d", &b);
    temp = a;
    a = b;
    b = temp;
    printf("Resultados: \na) A = %d\nb) B = %d\n", a, b);
    return 0;
}
