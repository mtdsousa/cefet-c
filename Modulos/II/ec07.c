/*

Sabe-se que o valor de cada 1000 litros de água corresponde a 2% do salário
mínimo. Faça um programa para receber o valor do salário mínimo e a
quantidade de água consumida em uma residência por mês. O algoritmo deverá
calcular e mostrar: a) o valor da conta de água. b) o valor a ser pago com
desconto de 15%.

1000l = 0.02 (2%)
1l = 0.00002

f(1) = 1x0.00002xSalario
f(x) = Xx0.00002xSalario

*/

#include <stdio.h>
int main(){
    float salarioMinimo, aguaConsumida, contaAgua, contaAguaDesconto;
    printf("Entre: \n1. Valor do salário mínimo: ");
    scanf("%f",&salarioMinimo);
    printf("2. Água Consumida: ");
    scanf("%f", &aguaConsumida);
    contaAgua = aguaConsumida*(0.00002*salarioMinimo);
    contaAguaDesconto = contaAgua*0.85;
    printf("Resultado:\na)%.2f\nb)%.2f\n", contaAgua, contaAguaDesconto);
    return 0;
}
