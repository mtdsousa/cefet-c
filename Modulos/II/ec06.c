/*

Faça um programa para calcular o número de vértices de um cubo com 6 faces
e 12 arestas. A relação entre vértices, arestas e faces de um objeto geométrico
é dada por: vértices + faces = arestas + 2.

*/

#include <stdio.h>
int main(){
    int vertices, faces = 6, arestas = 12;
    vertices = (arestas+2) -faces;
    printf("%d\n", vertices);
    return 0;
}

