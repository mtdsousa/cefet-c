/*

Num triângulo retângulo, segundo Pitágoras, o quadrado da hipotenusa (a) é
igual a soma dos quadrados dos catetos (b e c). Faça um algoritmo que recebe
o valor dos catetos e imprime o valor da hipotenusa.

*/

#include <stdio.h>
#include <math.h>
int main(){
    int a, b, c;
    printf("Entre: \n1. Cateto b: ");
    scanf("%d", &b);
    printf("2. Cateto c: ");
    scanf("%d", &c);
    a = sqrt(pow(b,2)+pow(c,2));
    printf("Resultado: \na) A = %d\n", a);
    return 0;
}
