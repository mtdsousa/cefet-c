/*

Escreva um programa que solicite ao usuário uma determinada data no formato aaaa-mm-dd e a mostre em seguida no formato dd/mm/aaaa.

*/

#include <stdio.h>
int main(){
    int dia, mes, ano;
    printf("Entre: \nData [aaaa-mm-dd]: ");
    scanf("%d-%d-%d", &dia, &mes, &ano);
    printf("Resultado: \na) %d/%d/%d\n", dia, mes, ano);
    return 0;
}
