#include <stdio.h>

float max(float v[], int n);

int main(){
	float v[10] = {0, 1, 2, 9, 5, 6, 7, 8, 1, 0};
	printf("%f\n\n", max(v, 10));
	return 0;
}

float max(float v[], int n){
	float maior = v[0];
	int  i = 0;
	for (i = 1; i<n; i++){
		if (v[i]>maior)
			maior = v[i];
	}
	return maior;

}
