#include <stdio.h>

#define DIM 3
#define ESPACO ' '


void inic(char s[ ][DIM]);
void mostra(char s[DIM][DIM]);
int confere(char s[DIM][DIM]);

int main(){
	char Velha[DIM] [DIM];
	int posx, posy;
	char ch = 'O';
	int n_jogadas = 0;

	inic(Velha);
	while(1){
		mostra(Velha);
		printf("\nIntroduza a Posição de Jogos Linha Coluna: ");
		scanf("%d %d", &posx, &posy);
		posx--;
		posy--;
		if (Velha[posx][posy] == ESPACO){
			Velha[posx][posy] = ch = (ch == 'O')? 'X' : 'O';
			if (confere(Velha)){
				mostra(Velha);
				printf("Jogador %c ganhou. \n", ch);
				break;
			}		
			n_jogadas++;
		}else{
			//printf("Posição já ocupada. \nJogue Novamente!!!\n");
		}
		if(n_jogadas == DIM*DIM){
			mostra(Velha);
			printf("Empate. \n");
			break;
		}
	}
	return 0;
}

void inic(char s[ ][DIM]){
	int i, j;
	for (i =0; i<DIM; i++){
		for (j=0; j<DIM; j++){
			s[i][j] = ESPACO;
		}
	}
}

void mostra(char s[DIM][DIM]){
	int i, j;
	system("clear");
	for (i = 0; i<DIM; i++){
		for (j=0; j<DIM; j++){
			printf("%c %c", s[i][j], j==DIM-1? ' ' : '|');
		}
		if (i!= DIM-1){
			printf("\n--------");
		}
		putchar('\n');
	}
}

int confere(char s[DIM][DIM]){
	int i;
	for (i =0; i<DIM; i++){
		if ((s[i][0] == s[i][1]) && (s[i][0] == s[i][2]) && (s[i][0] != ' '))
			return 1;
		
		if ((s[0][i] == s[1][i]) && (s[0][i] == s[2][i]) && (s[0][i] != ' '))
			return 1;
	}
	if ((s[0][0] == s[1][1]) && (s[0][0] == s[2][2]) && (s[0][0]!= ' '))
		return 1;
	if ((s[0][2] == s[1][1]) && (s[0][2] == s[2][0]) && (s[0][2] != ' '))
		return 1;
	return 0;
}
