#include <stdio.h>
#define MAX 3

void transpor(int v[MAX] [MAX]);
void printm(int v[MAX] [MAX]);

int main(){
	int matriz[MAX][MAX] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
	printm(matriz);
	transpor(matriz);
	putchar('\n');
	printm(matriz);
	return 0;
}

void transpor(int v[MAX][MAX]){
	int i, j, tmp;
	
	for (i=0; i<MAX; i++){
		for (j=i + 1; j<MAX; j++){
			tmp = v[i][j];
			v[i][j] = v[j][i];
			v[j][i] = tmp;	
		}
	}
}

void printm(int v[MAX][MAX]){
	int i, j;

	for (i=0; i<MAX; i++){
		for (j=0; j<MAX; j++){
			printf("%d ", v[i][j]);	
		}
		putchar('\n');
	}
}
