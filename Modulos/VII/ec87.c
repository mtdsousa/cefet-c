/*

"char *strstr (char *str1, char *str2)" que retorna o endereço de str1
em que ocorre pela primeira vez a substring str2. Caso não exista, retorna
NULL.

*/

#include <stdio.h>
#include <stdlib.h>

char *strstr (char *str1, char *str2);

int main(){
    char *str = "Quem vigiara os vigilantes?";
    printf("Resultado: \n1. %p\n", strstr(str, "vigi"));    
    return 0;
}

char *strstr (char *str1, char *str2){
    int i, j;
    char *p = NULL;
    for(i=0; str1[i]!='\0'; i++){
        for(j=0; str2[j]!='\0'; j++){
            if(str1[i+j]!=str2[j]){
                break;
            }
            p = &str1[i];   
        }
    }
    return p;
}
