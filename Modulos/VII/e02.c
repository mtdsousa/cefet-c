#include <stdio.h>
#include <ctype.h>
#include <string.h>

int strcounta(char *s);
char * init_str(char *s);
int ult_ind_chr(char *s, char c);
char * strlwr(char *str);
char * strnset(char *s, char ch, int n);
int stricmp(char *s1, char *s2);

int main(){
    char *s = "15 Abacates";
    char f[100] = "Ola Mundo";
    char q[100] = "Quem vigiará os guardiões?";
    char *test = "ALFA";
    char teste[100] = "ALFA";

    printf("%d\n", strcount(s));
    printf("%s\n", init_str(q));
    printf("%d\n", ult_ind_chr(s, 'a'));
    printf("%d\n", ult_ind_chr(s, 'i'));
    printf("%s\n", strlwr(f));
    printf("%s\n", strnset(f, '*', 3));	
    printf("%d\n", stricmp(s, "15 abacates"));
    printf("%d\n", stricmp(s, "16 Abacates"));
    return 0;
}

int strcount(char *s){
    int i, c = 0;
    for (i=0; s[i]!= '\0'; i++){
        if (toupper(s[i])>=65 && toupper(s[i]) <=90){
            c++;
        }
    }
    return c;	
}

char * init_str(char *s){
    s[0] = '\0';
    return s;
}

int ult_ind_chr(char *s, char c){
    int i, f= -1;
    for(i=0; s[i]!='\0'; i++){
        if (s[i] == c){
            f = i;
        }
    }
    return f;
}

char * strlwr(char *str){
    int i;
    for(i=0; str[i]!='\0'; i++){
        if(str[i]>=65 && str[i]<=90){
            str[i] += 32;
        }
    } 
    return str;
}

char * strnset(char *s, char ch, int n){
    if (n>strlen(s)){
        n = strlen(s);
    }
    int i;
    for(i=0; i<n; i++){
        s[i] = ch;
    }
    return s;
}

int stricmp(char *s1, char *s2){
    int i;
    for(i=0; s1[i]!='\0'; i++){
        if (tolower(s1[i]) != tolower(s2[i])){
            return 0;
        }
    }
    return 1;
}
