/*

"char * strchar (char *s, char ch)" que retorna o endereço da
primeira ocorrência de “ch” em “s”; caso não exista, retorna NULL (NULL é uma
constante simbólica que indica que o ponteiro não aponta para nenhuma
variável). Implemente a função principal (“main”) para testar a função strchar.

*/

#include <stdio.h>
#include <stdlib.h>

char * strchar (char *s, char ch);

int main(){
    char str[50] = "Programacao em C";
    printf("Entrada:\n1. programacao em c\n\nResultado:\n1. %p\n", strchar(str, 'c'));  
    return 0;
}

char * strchar (char *s, char ch){
    int i;
    for(i=0; s[i]!='\0'; i++){
        if(s[i]==ch){
            return &s[i];
        }   
    }
    return NULL;
}
