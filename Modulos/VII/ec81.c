/*

Uma fábrica produz dois tipos de motores M1 e M2. Nos meses de janeiro
a dezembro o número de motores produzidos foi registrado na tabela 1. O setor
de controle de vendas tem uma tabela de custos e de lucro (em mil reais)
obtidos com cada motor. Escreva um programa preenche as tabelas 1 e 2 com
números aleatórios e, em seguida calcula:

a) O lucro em cada um dos meses;
b) O lucro anual total.

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define M 2

void geraProducao(int t1[12][M]){
    int i, j;   
    for(i=0; i<12; i++){
        for(j=0; j<M; j++){
            t1[i][j] = rand()%1000; 
        }
    }   
}

void geraCustos(int t2[2][M]){
    int i, j;
    for(i=0; i<2; i++){
        for(j=0; j<M; j++){
            t2[i][j] = rand()%1000; 
        }
    }
}

void imprime(int t[][M], int a){
    int i, j;
    for(i=0; i<a; i++){
        for(j=0; j<M; j++){
            printf("%d ", t[i][j]);
        }
        putchar('\n');
    }
    putchar('\n');
}

int lucro(int t1[][M],int t2[][M], int mes){
    mes--;
    int i, j;
    int custo = 0, lucro = 0;
    if(mes==12){
        for(i=0; i<12; i++){
            custo += (t1[i][0]*t2[0][0]); //M1
            custo += (t1[i][1]*t2[0][1]); //M2

            lucro += (t1[i][0]*t2[1][0]); //M1
            lucro += (t1[i][1]*t2[1][1]); //M2
        }
    }else{
        custo += (t1[mes][0]*t2[0][0]); //M1
        custo += (t1[mes][1]*t2[0][1]); //M2

        lucro += (t1[mes][0]*t2[1][0]); //M1
        lucro += (t1[mes][1]*t2[1][1]); //M2
    }

    lucro -= custo;
    return lucro;
}

int main(){
    int t1[12][M], t2[M][2];
    srand((unsigned)time(NULL));
    geraProducao(t1);
    geraCustos(t2);
    imprime(t1, 12);
    imprime(t2, 2);
    printf("Resultado: \n1. Lucro em Julho: %d\n2. Lucro anual: %d\n\n", lucro(t1, t2, 7), lucro(t1, t2, 13));  
    return 0;
}
