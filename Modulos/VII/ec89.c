/*

"void eliminar(char * v, char ch)" que elimina todas as ocorrências do
caracter "ch" no vetor de caracteres "v". Ao eliminar um caracter, os outros
elementos do vetor devem ser movidos para esquerda. Por exemplo: v =
"programacao de computadores" e ch = 'a'. Ao final do procedimento o
conteúdo de "v" deverá ser "progrmco de computdores". Você não deve utilizar
um vetor auxiliar para implementar o procedimento.

*/

#include <stdio.h>
void eliminar(char * v, char ch);

int main(){
    char str[100] = "Programacao de Computadores";
    eliminar(str, 'a');
    printf("Resultado:\n1. %s\n", str); 
    return 0;
}

void eliminar(char * v, char ch){
    int i, j=0;
    for(i=0; v[i]!='\0'; i++){
        if(v[i]!=ch){
            v[j] = v[i];
            j++;
        }
    }
    v[j] = '\0';
}


