/*

"void calc(int * v, int num, int * xmin, int * xmax)" que coloca nas
posições de memórias apontadas por "xmin" e "xmax" o menor e o maior valor
(respectivamente) existentes nos "num" primeiros inteiros do vetor "v".

*/

#include <stdio.h>
#include <time.h>
#define L 10

void calc(int * v, int num, int * xmin, int * xmax){
    int i, min, max;
    min = max = v[0];   
    for(i=0; i<num; i++){
        if(v[i]>max){
            max = v[i];
        }
        if(v[i]<min){
            min = v[i];
        }
    }
    *xmin = min;
    *xmax = max;
}

void preenche(int * v){
    int i;
    for(i=0; i<L; i++){
        v[i] = rand()%100;
    }
}

void print(int *v){
    int i;
    for(i=0; i<L; i++){
        printf("%d ", v[i]);
    }
    putchar('\n');
}


int main(){
    srand((unsigned)time(NULL));
    int v[L], max, min;
    preenche(v);
    print(v);
    calc(v, L/2, &min, &max);
    printf("Resultado:\n1. Máximo: %d\n2. Mínimo: %d\n\n", max, min);
    return 0;
}

