/*

Implemente um programa que declara uma matriz 5x5 e armazena em
um vetor o maior elemento cadastrado em cada coluna da matriz e em outro
vetor o menor elemento cadastrado em cada coluna da matriz. Ao final, o
programa deve exibir: a matriz original, o vetor dos maiores elementos e o
vetor dos menores elementos.

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define LEN 5

void mrand(int m[][LEN], int mod){
    int i, j;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            m[i][j] = rand()%mod;
        }
    }
}

void printm(int m[][LEN]){
    int i, j;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            printf("%d ", m[i][j]);
        }
        putchar('\n');
    }
    putchar('\n');
}

void printa(int a[LEN]){
    int i;
    for(i=0; i<LEN; i++){
        printf("%d ", a[i]);
    }
    putchar('\n');
}

void max(int m[][LEN], int *maxa){
    int i, j, maxn = 0;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            if(!j || m[j][i]>maxn){
                maxn = m[j][i];
            }
        }
        maxa[i] = maxn;
        maxn = 0;
    }
}

void min(int m[][LEN], int *mina){
    int i, j, minn = 0;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            if(!j || m[j][i]<minn){
                minn = m[j][i];
            }
        }
        mina[i] = minn;
        minn = 0;
    }
}

int main(){
    srand((unsigned)time(NULL));
    
    int m[LEN][LEN];
    int maxa[LEN];
    int mina[LEN];

    mrand(m, 100);
    max(m, maxa);
    min(m, mina);
    
    printm(m);
    printa(maxa);
    printa(mina);
    return 0;
}
