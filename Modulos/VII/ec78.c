/*

Escreva um programa que carrega uma matriz de 12x4 com os valores
das vendas de uma loja (estes valores podem ser gerados aleatoriamente).
Cada linha da matriz representa um mês do ano e cada coluna, uma semana do
mês. Os dados devem ser processados de forma a produzir as seguintes
informações:

1. Total vendido em cada mês do ano;
2. Total vendido no ano.

Implemente uma função para cada um dos itens acima.

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define M 12
#define S 4

void mrand(int m[][S], int mod){
    int i, j;
    for(i=0; i<M; i++){
        for(j=0; j<S; j++){
            m[i][j] = rand()%mod;
        }
    }
}

void printm(int m[][S]){
    int i, j;
    for(i=0; i<M; i++){
        for(j=0; j<S; j++){
            printf("%d ", m[i][j]);
        }
        putchar('\n');
    }
    putchar('\n');
}

int vmes(int mes, int vendas[][S]){
    int i, v=0;
    for(i=0; i<S; i++){
        v+= vendas[mes-1][i];
    }
    return v;
}

int vano(int vendas[][S]){
    int i, v=0;
    for(i=0; i<M; i++){
        v+= vmes(i+1, vendas);
    }
    return v;
}

int main(){
    srand((unsigned)time(NULL));
    int vendas[M][S];
    mrand(vendas, 100);
    printm(vendas);
    printf("Resultado:\n1. Vendas no mês 7: %d\n2. Vendas no ano: %d\n", vmes(7, vendas), vano(vendas));
}

