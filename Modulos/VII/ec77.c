/*

Escreva um programa para ler duas matrizes (3x3) e gravar a soma das
duas em uma terceira matriz.

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define LEN 3

int * soma(int a[][LEN], int b[][LEN], int soma[][LEN]){
    int i, j;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            soma[i][j] = a[i][j]+b[i][j];
        }
    }
    return soma;
}

void mrand(int m[][LEN], int mod){
    int i, j;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            m[i][j] = rand()%mod;
        }
    }
}

void printm(int m[][LEN]){
    int i, j;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            printf("%d ", m[i][j]);
        }
        putchar('\n');
    }
    putchar('\n');
}

int main(){ 
    srand((unsigned)time(NULL));
    int *a[LEN][LEN];
    int *b[LEN][LEN];
    int *s[LEN][LEN];
    mrand(a, 100);
    mrand(b, 100);
    printm(a);
    printm(b);
    printf("Resultado:\n1. Soma: \n\n");
    printm(soma(a, b, s));
    return 0;
}

