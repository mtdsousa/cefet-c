/*

Implemente um procedimento que coloca em ordem crescente os
caracteres de um string “s” passado como parâmetro.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * sort(char *s){
    int i, j, aux;
    for(i=0; s[i]!='\0'; i++){
        for(j=i+1; s[j]!='\0'; j++){
            if(s[i]>s[j]){
                aux = s[i];
                s[i] = s[j];
                s[j] = aux;
            }   
        }
    }
    return s;
}

int main(){
    char str[50] = "programacao em c";
    printf("Entrada: \n1. Programação em C\n\nResultado:\n1. %s\n", sort(str));
    return 0;
}
