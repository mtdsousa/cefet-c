/*

Escreva um programa que armazena 16 valores em matriz 4x4. Os
números deverão ser gerados aleatoriamente. A matriz deverá ser uma variável
local dentro da função "main". Em seguinda, implemente funções que recebem
a matriz como parâmetro e retornam:

1. A quantidade de números pares da matriz;
2. A soma dos números ímpares da matriz;
3. A quantidade de números com valor maior do que a média dos números da
matriz;
4. O maior valor da matriz;
5. O segundo maior valor da matriz;
Implemente um procedimento para:
6. Eliminar os números repetidos da matriz. Os números repetidos devem ser
substituídos por novos números. Este procedimento deverá exibir na tela
todos os elementos da matriz, antes e depois de eliminar os números
repetidos.
7. Após implementar todas a funções e procedimentos acima, você deverá
implementar um menu onde o usuário poderá escolher uma das opções
acima (1—6). Adicione a opção “7” para sair do programa.

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define LEN 4

void printm(int m[][LEN]){
    int i, j;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            printf("%d ", m[i][j]);   
        }
        putchar('\n');
    }
}

int pares(int m[][LEN]){
    int i, j, c = 0;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            if(m[i][j]%2==0){
                c++;
            }
        }
    }
    return c;
}

int simpares(int m[][LEN]){
    int i, j, s = 0;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            if(m[i][j]%2!=0){
                s+=m[i][j];
            }
        }
    }
    return s;
}

int media(int m[][LEN]){
    int i, j;
    float md=0;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            md+=m[i][j];
        }
    }
    md /= (LEN*LEN);
    return md;
}

int mmedia(int m[][LEN]){
    int i, j, c=0, md;
    md = media(m);
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            if(m[i][j]>md){
                c++;
            }
        }
    }
    return c;
}

int max(int m[][LEN]){
    int i, j, maior;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            if(!i && !j){
                maior=m[i][j];
                continue;
            }
            if(m[i][j]>maior){
                maior = m[i][j];
            }
        }
    }
    return maior;
}

int smax(int m[][LEN]){
    int i, j, maior, sm=-1;
    maior = max(m);
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            if(m[i][j]!=maior){
                if(sm==-1 || m[i][j]>sm){
                    sm=m[i][j];
                }
            }
        }
    }
    return sm;
}

int exist(int m[][LEN], int n){
    int i, j;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            if(m[i][j]==n){
                return 1;
            }
        }
    }
    return 0;
}

void elimina(int m[][LEN], int mod){
    printm(m);
    srand((unsigned)time(NULL));
    int i, j, k, l, n;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            for(k=i; k<LEN; k++){
                for(l=j+1; l<LEN; l++){
                    while(m[i][j]==m[k][l]){
                        do{
                            n = rand()%mod;
                        }while(exist(m, n));
                        m[k][l] = n;
                    }
                }
            } 
        }
    }
    putchar('\n');
    printm(m);
}

int choice(){
    int option;
    printf("Entre:\na) Uma das opções abaixo:\n\n");
    printf("1. Quantidade de Pares\n2. Soma dos ímpares\n3. Maiores que a média\n4. Maior\n5. Segundo maior\n6. Eliminar repetidos\n7. Sair\n\n");
    scanf("%d", &option);
    return option;
}

int main(){
    srand((unsigned)time(NULL));
    int m[LEN][LEN];
    int i, j, op;
    for(i=0; i<LEN; i++){
        for(j=0; j<LEN; j++){
            m[i][j] = rand()%100;            
        }
    }
    
    do{
        op = choice();
        system("clear");
        switch(op){
            case 1:
                printf("1. Quantidade de pares: %d. ", pares(m));
                break;
            case 2:
                printf("2. Soma de impares: %d. ", simpares(m));
                break;
            case 3:
                printf("3. Maiores que a média: %d. ", mmedia(m));
                break;
            case 4:
                printf("4. Maior: %d. ", max(m));
                break;
            case 5:
                printf("5. O segundo maior: %d. ", smax(m));
                break;
            case 6:
                printf("6. Eliminando números repetidos:\n");
                elimina(m, 100);
                break;
        }
    }while(op!=7);
    return 0;
}

