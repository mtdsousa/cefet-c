#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char * repeticoes(char *s);
char n_esimo(char *s, int n);
char * strpack(char *s);
char * Entremeado(char *s, int n);
char * xspace(char *s);
char Max_Ascii(char *str);
char * Prox_Char(char *s);
char * UpDown(char *s);
char * allspaces(char *s);
char * strijset(char *s, int i, int j, char ch);
char * strduplica(char *s);
int atoi_(char *s);
char * wordupr(char *s);
char * lower_upper(char *s);
char * All_Big(char *s);
int Is_Len_OK(char *string, int comprimento);
int Is_Alfa_Digit(char *s);
char * Transform(char *s);

int main(){
    char s[100] = "42 e um numero legal.";
    char w[100] = "Alface";
    char t[100] = "Arrecadaccccaaao";
    char alfa[50] = "ABCDEFGHIJKLM";
   
    printf("%s\n", repeticoes(w));
    printf("%c\n", n_esimo(s, 2));
    printf("%s\n", strpack(t));
    printf("%s\n", Entremeado(t, 2));
    printf("%s\n", xspace(s));
    printf("%c\n", Max_Ascii(s));
    printf("%s\n", Prox_Char(alfa));
    printf("%s\n", UpDown(s));
    
    return 0;
}

char * repeticoes(char *s){
    //Resolução usa strcountc()
    char r[100];
    int i, j, n=0;
    for(i=0; s[i]!='\0'; i++){
        for(j=0; s[j]!='\0'; j++){
            if((tolower(s[i])==tolower(s[j]))&&(i!=j)){
                r[n] = s[i];
                n++;
            }	
        }	
    }
    r[n] = '\0';
    strcpy(s, r);
    return s;
}

char n_esimo(char *s, int n){
    return s[n-1];
}

char * strpack(char *s){
    int i, n=0;
    char r[100];
    for(i = 0; i<strlen(s); i++){
        if(i==0 || s[i] != s[i-1]){
            r[n] = s[i];
            n++;
        }   
    }
    r[n] = '\0';
    strcpy(s, r);
    return s;
}

char * Entremeado(char *s, int n){
    int i, j = 0;
    for(i=0; i<strlen(s); i+=n+1){
        s[j++] = s[i]; 
    }
    s[j] = '\0';
    return s;
}

int strcountc(char *s, char ch){
    int i, c = 0;
    for(i=0; s[i]!='\0'; i++){
        if(s[i]==ch){
            c++;
        }
    }
    return c;
}

char * xspace(char *s){
    //Livro texto:
    int i = strlen(s);
    int espacos = strcountc(s, ' '); 
    int dim = 2*i-espacos;

    s[dim--] = '\0';
    for (--i; i>=0; i--){
        if(s[i] == 32){
            s[dim--] = s[i];    
        }else{
            s[dim--] = 32;
            s[dim--] = s[i];    
        }
    }
    return s;
}


char Max_Ascii(char *str){
    //Livro texto:
    int i;
    char res='\0';
    for(i=0; str[i]!='\0'; i++){
        res = res > str[i] ? res : str[i];    
    }
    return res;
}

char * Prox_Char(char *s){
   //Livro texto:
   int i;
   for(i=0; s[i]!='\0'; i++){
       s[i] = s[i]+1;
   }
   return s;
}

char * UpDown(char *s){
    //Livro texto:
    int i;
    for(i=0; s[i]!= '\0'; i++){
        if(i%2==0){
            s[i] = toupper(s[i]);    
        }else{
            s[i] = tolower(s[i]);    
        }
    }
    return s;
}

char * allspaces(char *s){
    //Livro texto:
    //Poderíamos utilizar também: return strset(s, '')
    int i;
    for(i=0; s[i]!='\0'; i++){
        s[i]=' ';
    }
    return s;
}

char * strijset(char *s, int i, int j, char ch){
    //Livro texto: 
    if(i>=strlen(s)){
        return s;
    }
    while(i<=j && s[i]!= '\0'){
        s[i++] = ch;
    }
    return s;
}

char * strduplica(char *s){
    //Livro texto: 
    int i, len=strlen(s);
    for(i=0; i<len; i++){
        s[i+len] = '\0';    
    }   
    return s;
}
int atoi_(char *s){ //atoi() existe em <stdlib.h>
    //Livro texto:
    int i=0, res=0, sinal=1;
    if(s[0]=='-' || s[0]=='+'){
        i=1;
    } 
    if(s[0]=='-'){
        sinal = -1;    
    }
    for(; isdigit(s[i]); i++){
        res = res * 10 + s[i] - '0';   
    }
    return res*sinal;
}

char * wordupr(char *s){
    //Livro texto:
    int i;
    s[0] = toupper(s[0]);
    for(i=1; s[i-1]!='\0'; i++){
        if(s[i-1]==' '){
            s[i] = toupper(s[i]);    
        }else{
            s[i] = tolower(s[i]);    
        }
    }
    return s;
}

char * lower_upper(char *s){
    //Livro texto:
    int i, existem_trocas=1, len=strlen(s);
    char tmp;
    
    while(existem_trocas){
        existem_trocas=0;
        for(i=0; i<len-1; i++){
            if(isupper(s[i]) && islower(s[i+1])){
                tmp=s[i];
                s[i] = s[i+1];
                s[i+1]=tmp;
                existem_trocas=1;                
            }    
        }
    }    
    return s;
}

char * All_Big(char *s){
    //Livro texto:
    int i, j;
    for(i=j=0; s[i]!='\0'; i++){
        if(isupper(s[i])){
            s[j++]=s[i];    
        }    
    }
    s[j]=0;
}

int Is_Len_OK(char *string, int comprimento){
    //Livro texto:
    return strlen(string)==comprimento;
}

int Is_Alfa_Digit(char *s){
    //Livro texto:
    int i;
    for(i=0; s[i]!='\0'; i++){
        if(i%2==0){
            if(!isalpha(s[i])){
                return 0;
            }
        }else{
            if(!isdigit(s[i])){
                return 0;
            }
        }    
    }    
    return 1;
}
char * Transform(char *s){
    //Livro texto:
    int i, len=strlen(s)-1;
    for(i=0; i<=len; i++, len--){
        s[i] = tolower(s[i]);
        s[len] = toupper(s[len]);
    }
    return s;
}
