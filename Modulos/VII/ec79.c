/*

Uma empresa tem registrado em uma tabela os consumos mensais de
energia elétrica dos anos de 2003 até 2010. Cada linha representa um ano e
cada coluna representa um mês. Considerando esses dados, faça um algoritmo
para processar a tabela e produzir as seguintes informações:
1. Consumo médio em cada um dos meses;
2. Mês/ano em que houve o maior gasto com energia.

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define A 8
#define M 12

void mrand(int m[][M], int mod){
    int i, j;
    for(i=0; i<A; i++){
        for(j=0; j<M; j++){
            m[i][j] = rand()%mod;
        }
    }
}

void printm(int m[][M]){
    int i, j;
    for(i=0; i<A; i++){
        for(j=0; j<M; j++){
            printf("%d ", m[i][j]);
        }
        putchar('\n');
    }
    putchar('\n');
}

float mconsumo(int mes, int consumo[][M]){
    int i;
    float c;
    for(i=0; i<A; i++){
        c += consumo[i][mes-1];
    }
    return (c/A);
}

void max(int consumo[][M], int *mes, int *ano){
    int i, j, m = -1;
    for(i=0; i<A; i++){
        for(j=0; j<M; j++){
            if(m==-1 || consumo[i][j]>m){
                m = consumo[i][j];
                *ano = 2003+i;
                *mes = j+1;
            }
        }
    }
}

int main(){
    srand((unsigned)time(NULL));
    int consumo[A][M];
    
    int maxmes;
    int maxano;
    float media;
    
    mrand(consumo, 100);
    media = mconsumo(7, consumo);
    max(consumo, &maxmes, &maxano);
    printm(consumo);
    printf("Resultado:\n1. Média de consumo no mês 7: %.2f\n2. Maior consumo: mês %d de %d\n", media, maxmes, maxano);
    return 0;    
}

