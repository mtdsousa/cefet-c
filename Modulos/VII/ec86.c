/*

"void imprime_sobrenome (char *nome_completo)" que recebe o
nome completo de uma pessoa e imprime o último nome. Por exemplo, para o
nome completo "Jose Maria da Silva", o programa deve exibir: "Silva".

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void imprime_sobrenome (char *nome_completo);

char * strrev(char *s){
    int i, j=0;
    char aux[100];
    for(i=strlen(s)-1; i>=0; i--){
        aux[j] = s[i];
        j++;
    }
    strcpy(s, aux);
    return s;
}

int main(){
    char nome[100] = "Wolfgang Amadeus Mozart";
    imprime_sobrenome(nome);
}

void imprime_sobrenome (char *nome_completo){
    int i;
    int j = 0;
    char n[100];
    char sn[100];
    strcpy(n, nome_completo);
    for(i=strlen(n)-1; n[i]!=' '; i--){
        sn[j] = n[i];
        j++;
    }
    sn[j] = '\0';
    strrev(sn);
    printf("Resultado:\n1. %s\n", sn);
}
