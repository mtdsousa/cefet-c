/*

Escreva um programa para armazenar 15 números inteiros em um vetor. Os
números deverão ser gerados aleatoriamente. O vetor deverá ser uma variável
local dentro da função “main”. Em seguida, implemente funções que recebem o
vetor como parâmetro e retornam:

1. A quantidade de números pares do vetor;
2. A soma dos números ímpares do vetor;
3. O maior valor do vetor;
4. O segundo maior valor do vetor;
5. A quantidade de números com valor maior do que a média dos números do
vetor.
6. A maior diferença em valor absoluto entre os elementos consecutivos do
vetor;

Implemente um procedimento para:
7. Eliminar números repetidos do vetor. Os números repetidos devem ser
substituídos por novos números. Este procedimento deverá exibir na tela
todos os elementos do vetor, antes e depois de eliminar os números
repetidos.
8. Após implementar todas a funções e procedimentos acima, você deverá
implementar um menu onde o usuário poderá escolher uma das opções
acima (1—7). Adicione a opção “8” para sair do programa

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define LEN 15

void printa(int *a, int len){
    int i;
    for(i=0; i<len; i++){
        printf("%d ", a[i]);
    }
    putchar('\n');
}

int pares(int *a, int len){
    int i, c=0;
    for(i=0; i<len; i++){
        if(a[i]%2==0){
            c++;
        }
    }
    return c; 
}

int simpares(int *a, int len){
    int i, s =0;
    for(i=0; i<len; i++){
        if(!a[i]%2==0){
            s+=a[i];
        }
    }
    return s;
}

int max(int *a, int len){
    int i, m;
    for(i=1, m=a[0]; i<len; i++){
        if(a[i]>m){
            m = a[i];
        }
    }
    return m;
}

int smax(int *a, int len){
    int i, sm = -1, m;
    for(i=1, m = a[0]; i<len; i++){
        if(a[i]>m){
            m = a[i];
        }else{
            if(sm==-1 || a[i]>sm){
                sm = a[i];
            }
        }
    }
    return sm;
}

int mmedia(int *a, int len){
    int i, s=0;
    float m;
    for(i=0; i<len; i++){
        s+=a[i];
    }
    m = (float)s/len;
    for(i=0, s=0; i<len; i++){
        if(a[i]>m){
            s++;
        }
    } 
    return s;
}

int maxb(int *a, int len){
    int i, d = -1;
    for(i=0; i<len; i++){
        if(!i){
            continue;
        }
        if(d == -1 || abs(a[i]-a[i-1])>d){
            d = abs(a[i]-a[i-1]);
        }
    }
    return d;
}

int exist(int *a, int len, int n){
    int i;
    for(i=0; i<len; i++){
        if(a[i]==n){
            return 1;
        }
    }
    return 0;
}

void elimina(int *a, int len, int mod){
    printa(a, LEN);
    srand((unsigned)time(NULL));
    int i, j, n;
    for(i=0; i<len; i++){
        for(j=i+1; j<len; j++){
            while(a[i]==a[j]){
                n = rand()%mod;
                while(exist(a, LEN, n)){
                    n = rand()%mod;
                }
                a[j] = n;
            } 
        }
    }
    printa(a, LEN);
}

int choice(){
    int option;
    printf("Entre:\na) Uma das opções abaixo:\n\n");
    printf("1. Quantidade de Pares\n2. Soma dos ímpares\n3. Maior\n4. O segundo maior\n5. Maiores que a média\n6. Maior diferença entre consecutivos\n7. Eliminar repetidos\n8. Sair\n\n");
    scanf("%d", &option);
    return option;
}

int main(){
    int a[15], i, n, op;
    srand((unsigned)time(NULL));
    for(i=0; i<15; i++){
        a[i] = rand()%100;
    }
    do{
        op = choice();
        system("clear");
        switch(op){
            case 1:
                printf("1. Quantidade de pares: %d. ", pares(a, LEN));
                break;
            case 2:
                printf("2. Soma de impares: %d. ", simpares(a, LEN));
                break;
            case 3:
                printf("3. Maior: %d. ", max(a, LEN));
                break;
            case 4:
                printf("4. O segundo maior: %d. ", smax(a, LEN));
                break;
            case 5:
                printf("5. Maiores que a média: %d. ", mmedia(a, LEN));
                break;
            case 6:
                printf("6. Maior diferença entre consecutivos: %d. ", maxb(a, LEN));
                break;
            case 7:
                printf("7. Eliminando elementos repetidos: \n");
                elimina(a, LEN, 100);
                break;
        }
    }while(op!=8);
    return 0;
}
