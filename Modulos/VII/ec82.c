/*

Implemente um programa que coloca em ordem crescente os números
de um vetor de tamanho 40. O vetor não deve conter elementos repetidos e
deve ser preenchido com números aleatórios.

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define LEN 40

int existe(int v[LEN], int n);

void preenche(int v[LEN]){
    int i, n;
    for(i=0; i<LEN; i++){
        do{
            n = rand()%100;
        }while(existe(v, n));
        v[i] = n;
    }
}

int existe(int v[LEN], int n){
    int i;
    for(i=0; i<LEN; i++){
        if(v[i]==n){
            return 1;
        }
    }
    return 0;
}

void sort(int v[LEN]){
    int i, j, aux;
    for(i=0; i<LEN; i++){
        for(j=i+1; j<LEN; j++){
            if(v[i]>v[j]){
                aux = v[i];
                v[i] = v[j];
                v[j] = aux;
            }   
        }
    }   
}

void printm(int v[LEN]){
    int i;
    for(i=0; i<LEN; i++){
        printf("%d ", v[i]);
    }
    putchar('\n');
}

int main(){
    srand((unsigned)time(NULL));    
    int v[LEN];
    preenche(v);
    printm(v);
    sort(v);
    printm(v);
    return 0;
}
