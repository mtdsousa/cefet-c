/*

Implemente um programa para controlar o estoque de produtos de uma
empresa. Os produtos em estoque devem estar armazenados em um arquivo
ESTOQUE.TXT contendo os seguintes dados: número do produto (int), nome do
produto (string), quantidade em estoque (int), preço unitário (float), e
quantidade mínima (int). O programa deverá apresentar as seguintes opções ao
usuário:

(1) Inserir produto
(2) Alterar estoque
(3) Listagem de Compra.
(4) Listar todos.
(5) Fim

Inserir produto: deve-se perguntar o número do produto e verificar se o produto
já existe no arquivo. Caso exista, mostrar uma mensagem de erro. Caso não
exista, deve-se perguntar o nome do novo produto, a quantidade inicial do
produto em estoque, o valor unitário e a quantidade mínima que este produto
deve ter em estoque. Em seguida, deve-se acrescentar uma nova linha com
estes dados ao arquivo.

Alterar estoque: deve-se perguntar o número do produto e verificar se o
produto existe (caso não exista, mostrar uma mensagem de erro). Caso exista,
deve-se perguntar a quantidade em estoque a ser retirada. Em seguida, deve-
se verificar se a retirada ainda é possível (ou seja, a quantidade em estoque
deve ser maior ou igual à quantidade a ser retirada) e atualizar os valores. Caso
a retirada não seja possível, mostrar uma mensagem de erro.

Listar todos: Listar todos os produtos em ESTOQUE.TXT.

Listagem de compra: deve-se listar todos os produtos cuja quantidade em
estoque é menor do que a quantidade mínima.

*/

#include <stdio.h>
#include <string.h>
#define ESTOQUE "files/estoque.txt"

struct PRODUTO{
    int cproduto;
    char nproduto[100];
    int qtd;
    float preco;
    int qtdmin;
};
typedef struct PRODUTO Produto;

int verifica(Produto *produtos, int q, int cod){
    int i;
    for(i=0; i<q; i++){
        if(produtos[i].cproduto == cod){
            return 1;
        }
    }
    return 0;
}

int load(Produto *produtos){
    FILE *estoque = fopen(ESTOQUE, "r");
    int cproduto, qtd, qtdmin, i = 0;
    float preco;
    char nproduto[100];
    if(estoque == NULL){
        printf("\nErro: Não foi possível operar arquivos.\n");    
        return 0;       
    }
        
    while(fscanf(estoque, "%d %s %d %f %d", &cproduto, nproduto, &qtd, &preco, &qtdmin)!=EOF){
        produtos[i].cproduto = cproduto;
        strcpy(produtos[i].nproduto, nproduto);
        produtos[i].qtd = qtd;
        produtos[i].preco = preco;
        produtos[i].qtdmin = qtdmin;
        i++;    
    }
    fclose(estoque);
    return i;
}

int save(Produto *produtos, int q){
    int i;
    FILE *estoque = fopen(ESTOQUE, "w");
    if(estoque == NULL){
        printf("\nErro: Não foi possível operar arquivos.\n");
        return 0;    
    }
    for(i=0; i<q; i++){
        fprintf(estoque, "%d %s %d %f %d\n", produtos[i].cproduto, produtos[i].nproduto, produtos[i].qtd, produtos[i].preco, produtos[i].qtdmin);
    }
    fclose(estoque);
    return 1;    
}

int inserir(Produto *produtos, int q){
    int cproduto, qtd, qtdmin, i = 0;
    float preco;
    char nproduto[100];
    FILE *estoque = fopen(ESTOQUE, "a+");
    printf("\nInserir novo produto: \n1. Entre:\na) Código: ");
    scanf("%d", &cproduto);
    getchar();
    if(verifica(produtos, q, cproduto)){
        return 0;
    }
    printf("b) Nome do produto: ");
    gets(nproduto);
    printf("c) Quantidade em estoque: ");
    scanf("%d", &qtd);
    printf("d) Preço: ");
    scanf("%f", &preco);
    printf("e) Quantidade mínima em estoque: ");
    scanf("%d", &qtdmin);
    
    if(fprintf(estoque, "%d %s %d %f %d\n", cproduto, nproduto, qtd, preco, qtdmin)>0){
        produtos[q].cproduto = cproduto;
        strcpy(produtos[q].nproduto, nproduto);
        produtos[q].qtd = qtd;
        produtos[q].preco = preco;
        produtos[q].qtdmin = qtdmin;
    fclose(estoque);
        return 1;
    }
    fclose(estoque);    
    return 0;
}

int editar(Produto *produtos, int q){
    int qtd, cod, i;
    printf("Retirada de Estoque\n1. Entre:\na) Código do produto: ");
    scanf("%d", &cod);
    if (!verifica(produtos, q, cod)){
        printf("\nErro: Produto não encontrado. \n");
    }
    for(i=0; i<q; i++){
        if(produtos[i].cproduto==cod){
            printf("b) Valor da retirada de %ss: ", produtos[i].nproduto);
            scanf("%d", &qtd);
            if(qtd>produtos[i].qtd){
                printf("\nErro: Retirada de valor maior do que o disponível. \n");
                return 0;    
            }
            produtos[i].qtd -= qtd;
            return 1;
        }
    }
    return 0;
}

void listar(Produto *produtos, int q, int mod){
    int i;
    printf("Listando produtos:\nCOD\tNOME\tQUANTIDADE\tPRECO\tQTD MINIMA\n\n");
    for(i=0; i<q; i++){
        if(mod && produtos[i].qtd>=produtos[i].qtdmin){
                break;
        }
        printf("%d\t%s\t%d\t%f\t%d\n",produtos[i].cproduto, produtos[i].nproduto, produtos[i].qtd, produtos[i].preco, produtos[i].qtdmin);     
    }    
}

int main(int argc[], char argv[]){
    Produto produtos[100];
    int i = load(produtos);
    
    int op;
    do{
        printf("\n\nEntre:\n(1) Inserir produto\n(2) Alterar estoque\n(3) Listagem de Compra.\n(4) Listar todos.\n(5) Fim\n\n-> ");
        scanf("%d", &op);
        system("clear");
        switch(op){
            case 1:
                if(inserir(produtos, i)){
                    i++;    
                }else{
                    printf("\nErro: Não foi possível inserir novo produto.\n");
                }
                break;
            case 2:
                if(editar(produtos, i)){
                   save(produtos, i);     
                }else{
                    printf("\nErro: Não foi possível editar o produto. \n");    
                }
                break;
            case 3:
                listar(produtos, i, 1);
                break;
            case 4:
                listar(produtos, i, 0);
                break;
            default:
                op = 5;
                break;    
        }
    }   
    while(op<5);
    return 0;
}
