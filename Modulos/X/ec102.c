/*

Considere que um arquivo de dados contém os elementos de duas
matrizes 5x5. Implemente um programa que calcula e exibe a matriz resultante
da soma dessas duas matrizes.

*/

#include <stdio.h>

void print(int m[][5]){
    int i, j;
    for(i=0; i<5; i++){
        for(j=0; j<5; j++){
            printf("%d ", m[i][j]);    
        }
        putchar('\n');    
    }    
    putchar('\n');
}

int main(){
    FILE *file = fopen("files/matrizes.txt", "r");
    int res[5][5], i = 0, first = 1;
    int a, b, c, d, e;

    if(file==NULL){
        printf("Erro: Não foi possível operar com arquivos.");
        return 1;    
    } 
    while(fscanf(file, "%d %d %d %d %d", &a, &b, &c, &d, &e)!=EOF){
        res[i][0]=(first)?a:res[i][0]+a;
        res[i][1]=(first)?b:res[i][1]+b;
        res[i][2]=(first)?c:res[i][2]+c;
        res[i][3]=(first)?d:res[i][3]+d;
        res[i][4]=(first)?e:res[i][4]+e;
        
        i++;
        if (i>=5){
            first = 0;
            i = 0;
        }
    }
    print(res);
    return 0;    
}


