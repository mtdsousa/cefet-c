/*

Implemente um programa que abre um arquivo texto e conta quantas
vezes a letra "a" aparece no arquivo.

*/

#include <stdio.h>
int count(FILE *file, char find);

int main(){
    FILE *file = fopen("files/exemplo.txt", "r");
    if(file==NULL){
        printf("Erro: Não foi possível operar arquivos.");    
        return 1;
    }
    printf("Resultado:\n1. Há %d letras 'a'\n", count(file, 'a'));
    return 0;   
}

int count(FILE *file, char find){
    int c = 0;
    char ch;
    while((ch=fgetc(file))!=EOF){
        if(ch==find){
            c++;
        }     
    }
    return c;    
}
