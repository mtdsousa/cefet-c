/*

Uma empresa armazena seu cadastro de produtos no arquivo PRODUTO.TXT, cujas 
linhas armazenam os seguintes dados: número do produto(int), 
nome do produto (string), código do fornecedor (int) e preço (float). Esta
empresa armazena no arquivo FORNECE.TXT o código do fornecedor (int) e seu
nome (string). Implemente um programa que apresenta ao usuário o seguinte
menu:

1) Mostrar produto.
2) Listar todos produtos.
3) Fim

Enquanto o usuário escolher a primeira opção, o programa deve perguntar qual 
o número do produto a ser mostrado. Em seguinda, deve procurar pelo produto 
no arquivo PRODUTO.TXT e, caso exista, mostrar o nome do produto, o preço
e o nome do fornecedor (que se encontra no arquivo FORNECE.TXT). Note que o 
programa deve mostrar o nome do fornecedor (e não seu código). Caso o produto 
NÃO exista, o programa deverá mostrar a mensagem: "Produto xxx não encontrado".

Os arquivos PRODUTO.TXT e FORNCE.TXT podem ser preenchidos utilizando um editor
de textos.

*/

#include <stdio.h>
#include <string.h>

struct PRODUTO{
    int codigo;
    char nome[100];
    int fornecedor;
    float preco;
};
typedef struct PRODUTO produto;

struct FORNECEDOR{
    int codigo;
    char nome[100];
};
typedef struct FORNECEDOR fornecedor;

void busca(produto *produtos, fornecedor *fornecedores, int produto, int q, int q2);
void lista(produto *produtos, fornecedor *fornecedores, int q, int q2);

int main(){
    //Criando estruturas de produtos    
    FILE *fproduto = fopen("files/produto.txt", "r");
    if (fproduto==NULL){
        printf("Erro: Impossível operar arquivos. \n");
        return 1;
    }
    
    int pcod, cfornece;
    char nproduto[100];
    float preco;
    
    produto produtos[100];  
    int i = 0;  

    while(fscanf(fproduto, "%d %s %d %f", &pcod, nproduto, &cfornece, &preco)!=EOF){
        produtos[i].codigo = pcod;
        strcpy(produtos[i].nome, nproduto);
        produtos[i].fornecedor = cfornece;
        produtos[i].preco = preco;
        i++;
    }

    //Criando estruturas de fornecedores
    FILE *ffornecedor = fopen("files/fornece.txt", "r");
    if (ffornecedor==NULL){
        printf("Erro: Impossível operar arquivos. \n");
        return 1;
    }
    
    int fcod;
    char nfornecedor[100];
    fornecedor fornecedores[100];
    
    int j = 0;  
    while(fscanf(ffornecedor, "%d %s", &fcod, nfornecedor)!=EOF){
        fornecedores[j].codigo = fcod;
        strcpy(fornecedores[j].nome, nfornecedor);
        j++;
    }
    
    //Menu da aplicação
    int opcao;
    int cod;
    do{ 
        
        printf("Entre:\n1. Mostrar produto\n2. Listar todos os produtos\n3. Fim\n-> ");
        scanf("%d", &opcao);
        switch(opcao){
            case 1:
                system("clear");
                printf("Entre:\n1. Código do produto: \n-> ");
                scanf("%d", &cod);
                system("clear");
                busca(produtos, fornecedores, cod, i, j);           
                break;
            case 2:
                lista(produtos, fornecedores, i, j);
                break;
            default:
                opcao = 3;
                break;
        }
    }while(opcao!=3);
    return 0;
}

char * getFornecedor(fornecedor *fornecedores, int cod, char * result, int q){
    int i;

    for(i=0; i<q; i++){
        if(fornecedores[i].codigo == cod){
            strcpy(result, fornecedores[i].nome);
            return result;
        }
    }
}

void busca(produto *produtos, fornecedor *fornecedores, int cproduto, int q, int q2){   
    int i;
    char result[100];
    char nfornecedor[100];  
    
    for(i=0; i<q; i++){
        if(produtos[i].codigo == cproduto){
            strcpy(result, produtos[i].nome);
            printf("%s \nFornecido por: %s\nCusto: %f\n\n", result, getFornecedor(fornecedores, produtos[i].fornecedor, nfornecedor, q2), produtos[i].preco);
            return;
        }
    }
    char sproduto[15];
    sprintf(sproduto, "%d", cproduto);
    
    strcpy(result, "");
    strcat(result, "Produto ");
    strcat(result, sproduto);
    strcat(result, " não encontrado.");
    printf("%s\n\n", result);
}

void lista(produto *produtos, fornecedor *fornecedores, int q, int q2){
    int i;
    char result[100];
    char nfornecedor[100];
    for(i=0; i<q; i++){
        strcpy(result, produtos[i].nome);
        printf("%s \nFornecido por: %s\nCusto: %f\n\n", result, getFornecedor(fornecedores, produtos[i].fornecedor, nfornecedor, q2), produtos[i].preco);
    }
}



