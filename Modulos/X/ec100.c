/*

Implemente um programa que abre um arquivo texto "exemplo.txt" e
copia seu conteúdo para outro arquivo texto com o nome "destino.txt". O
arquivo "exemplo.txt" deve existir previamente na pasta onde está o executável
do programa.

*/

#include <stdio.h>

int main(){
    FILE *origem, *destino;
    char ch;
    origem = fopen("files/exemplo.txt", "r");
    destino = fopen("files/destino.txt", "w");
    if(origem==NULL || destino==NULL){
        printf("Erro: Não foi possível operar arquivos.\n");
        return 1;    
    }
    while((ch=fgetc(origem))!=EOF){
        fputc(ch, destino);    
    }
    return 0;
}


