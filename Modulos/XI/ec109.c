/*

Considere uma lista encadeada simples de valores inteiros. Implemente a 
função "separa" que recebe como parâmetro a lista encadeada e um valor 
inteiro n e divide a lista em duas, de tal forma que a segunda lista comece 
no primeiro nó logo após a primeira ocorrência de n na lista original. A 
função deve obedecer ao protótipo: Lista* separa (Lista* l, int n); A função
deve retornar um ponteiro para a segunda sub-divisão da lista original, e o
parâmetro "l" deve continuar apontando para o primeiro elemento da primeira
subdivisão da lista. Implemente o programa principal para testar sua função. A
lista deve ser alocada dinâmicamente e preenchida no programa principal.

*/

#include <stdio.h>
#include <stdlib.h>

struct LISTA{
    int n;
    struct LISTA * proximo;
};
typedef struct LISTA Lista;

Lista* separa (Lista * l, int n){
    Lista * new;
    while(l->proximo!=NULL){
        if(l->n==n){
            new = l->proximo;
            l->proximo = NULL; 
            return new;   
        }
        l = l->proximo;
    }
    return NULL;
}

void print(Lista * l){
    if(l==NULL){
        printf("NULL\n");
        return;
    }
    do{
        printf("%d ", l->n);
        l = l->proximo;
    }while(l!=NULL);
    putchar('\n');
}

Lista * insere(Lista * l, int n){
    Lista * new = (Lista *)malloc(1*sizeof(Lista));
    Lista * last = l;
    new->n = n;
    new->proximo = NULL;
    
    if(l==NULL){
        l = new;
        return l;
    }
    while(last->proximo!=NULL)
        last = last->proximo;    
    last->proximo = new;
    return l;
}

int main(){
    Lista * l = NULL;
    Lista * sl = NULL;
    l = insere(l, 1);
    insere(l, 3);
    insere(l, 10);
    insere(l, 11);
    insere(l, 2);
    insere(l, 98);
    sl = separa(l, 10);
    print(l);
    print(sl); 
    return 0;
}
