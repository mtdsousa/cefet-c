/*

Implemente um vetor de estruturas onde cada elemento é
constituído por um Número positivo e por um Nome (40 caracteres). O vetor
não poderá ter mais do que MAX registros (elementos). Implemente as
seguintes funções:

1. Inserir, que adiciona um registro ao vetor e retorna a posição em que foi
introduzido.
2. Salvar, que salva em arquivo binário o conteúdo do vetor.
3. Carregar, que faz a leitura dos registros existentes no arquivo e os insere no
vetor. Se o registro já existir no vetor, ele não deverá ser inserido.
4. Listar todos, que lista todos os elementos do vetor.
5. Remover registro, que solicita um Número e remove do vetor o registro
correspondente.

*/

#include <stdio.h>
#include <string.h>
#define MAX 10
#define DATA "files/elementos.dat"

struct ELEMENTO{
    int chave;
    char nome[40];
};
typedef struct ELEMENTO Elemento;

int size = 0;
Elemento elementos[MAX];

int existe(int chave){
    int i;
    for(i=0; i<size; i++){
        if(elementos[i].chave==chave){
            return 1;
        }
    }
    return 0;
}

int inserir(int chave, char * nome){
    if(!existe(chave)){
        elementos[size].chave = chave;
        strcpy(elementos[size].nome, nome);
        size++;
        return size-1;
    }
    return 0;
}

int salvar(){
    FILE *data = fopen(DATA, "wb");
    if(data==NULL)
        return 0;
    if (fwrite(elementos, sizeof(Elemento), size, data)>0){
        fclose(data);
        return 1;
    }
    fclose(data);
    return 0;
}

int carregar(){
    FILE *data = fopen(DATA, "rb");
    Elemento elementos_file[MAX];
    int n, i;
    if(data==NULL)
        return 0;
    long int a;
    fseek(data, (long)0, SEEK_END);
    a = ftell(data);
    a /= sizeof(Elemento);
    rewind(data);
    if((n = fread(elementos_file, sizeof(Elemento), a, data))>0){
        for(i=0; i<n; i++){
            if(!existe(elementos_file[i].chave)){
                inserir(elementos_file[i].chave, elementos_file[i].nome);
            }
        }
        fclose(data); 
        return 1;
    } 
    fclose(data);
    return 0;
}

void listar(){
    int i;
    for(i=0; i<size; i++){
        printf("%d %s\n", elementos[i].chave, elementos[i].nome);
    }
}

int remover(int chave){
    int i, find = 0;
    for(i=0; i<size; i++){
        if(elementos[i].chave == chave){
            find = 1;
            continue;
        }
        if(find){
            elementos[i-1].chave = elementos[i].chave;
            strcpy(elementos[i-1].nome, elementos[i].nome);
        }
    }    
    if(find){
        size--;
        return 1;
    }
    return 0;
}

int main(){
    int op, chave;
    char nome[100];
    carregar();
    do{
        printf("-> ");
        scanf("%d", &op);
        switch(op){
            case 1:
                printf("Inserir:\n1. Chave: ");
                scanf("%d", &chave);
                getchar();
                printf("2. Nome: ");
                gets(nome);
                inserir(chave, nome);
                salvar();
                break;
            case 2:
                listar();
                break;
            case 3:
                printf("Remover:\n1. Chave: ");
                scanf("%d", &chave);
                remover(chave);
                salvar();
                break;
            default:
                op = 5;
                break;
        }
    }while(op<4);
    return 0;    
}
