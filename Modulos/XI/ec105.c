/*

Declare uma estrutura (struct) em C para armazenar informações de um
produto de uma loja. Defina corretamente o tipo de cada campo da estrutura.
Nesta estrutura deve-se armazenar as seguintes informações: nome do produto,
preço e peso. O programa deve solicitar ao usuário que digite os valores para os
campos e, em seguida, exibi-los na tela.

*/

#include <stdio.h>

struct PRODUTO{
    char nome[100];
    float preco;
    float peso;
};
typedef struct PRODUTO Produto;

int main(){
    Produto p;
    printf("Entre:\n1. Nome: ");
    gets(p.nome);
    printf("2. Preço: ");
    scanf("%f", &p.preco);
    printf("3. Peso: ");
    scanf("%f", &p.peso);
    printf("\nResultado:\n1. Nome: %s\n2. Preço: %.2f\n3. Peso: %.2f\n\n", p.nome, p.preco, p.peso);
    return 0;    
}
