/*

Implemente uma agenda de contatos que armazena o "nome", "celular"
e "telefone". Salve os dados da agenda em arquivo binário. Durante a execução
do programa, os dados da agenda deverão ser manipulados utilizando-se uma
lista dinâmica. Ao iniciar o programa, carregue os dados do arquivo para a lista
dinâmica e, ao fechar o programa, salve as informações da lista no arquivo
binário. Implemente as funções de "incluir", "excluir" e "procurar" um contato.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DATA "files/agenda.dat"

struct CONTATO{
    char nome[100];
    int telefone;
    int celular;

    struct CONTATO * prox;
};
typedef struct CONTATO Contato;

Contato * incluir(Contato * agenda, Contato * add);
Contato * load(Contato * agenda);
int save(Contato * agenda);
void lista(Contato * agenda);
Contato * excluir(Contato * agenda, char * nome);
Contato * pesquisar(Contato * agenda, char * name);

Contato * load(Contato * agenda){
    FILE * data = fopen(DATA, "rb");
    Contato * copy = (Contato*)malloc(sizeof(Contato));
    if(data==NULL){
        printf("Erro: Agenda não encontrada. \n");
        return NULL;
    }
    while(fread(copy, sizeof(Contato), 1, data)==1){
        agenda = incluir(agenda, copy);
    }
    fclose(data);
    return agenda;
}

Contato * incluir(Contato * agenda, Contato * add){
    Contato * novo = (Contato *)malloc(sizeof(Contato));
    Contato * last = agenda;
    novo->prox = NULL;
    if(add!=NULL){
        strcpy(novo->nome, add->nome);
        if(pesquisar(agenda, novo->nome)!=NULL){
            printf("Erro: %s já existe. \n", novo->nome);
            return NULL;
        }
        novo->telefone = add->telefone;
        novo->celular = add->celular;
    }else{
        printf("Inserção\n");
        do{
            printf("1. Nome: ");
            gets(novo->nome);
            if(pesquisar(agenda, novo->nome)!=NULL){
                printf("Erro: %s já existe. \n", novo->nome);
            }
        }while(pesquisar(agenda, novo->nome)!=NULL);
        printf("2. Telefone: ");
        scanf("%d", &novo->telefone);
        getchar();
        printf("3. Celular: ");
        scanf("%d", &novo->celular);
    }
    if(last == NULL){
        agenda = novo;
        return agenda;
    }
    while(last->prox!=NULL){
        last = last->prox;
    }
    last->prox = novo;
    return agenda;
}

int save(Contato * agenda){
    remove(DATA);
    FILE * data = fopen(DATA, "ab");
    Contato * last = agenda;
    if(data==NULL){
        printf("Erro ao operar arquivos. \n");
        return 0;
    }
    if(agenda==NULL){
        return 1;
    }
    while(last->prox!=NULL){
        fwrite(last, sizeof(Contato), 1, data);
        last = last->prox;
    }
    fwrite(last, sizeof(Contato), 1, data);
    fclose(data);
    return 1;
}

void lista(Contato * agenda){
    Contato * contato = agenda;
    if(agenda==NULL){
        printf("Nenhum contato ainda. \n");
        return;
    }
    while(contato->prox!=NULL){
        printf("%s\n1. Telefone: %d\n2. Celular: %d\n\n", contato->nome, contato->telefone, contato->celular);
        contato = contato->prox;
    }
    printf("%s\n1. Telefone: %d\n2. Celular: %d\n\n", contato->nome, contato->telefone, contato->celular);
}

Contato * excluir(Contato * agenda, char * nome){
    Contato * contato = agenda;
    char read[100];
    if(nome==NULL){
        printf("Excluir:\n1. Entre com o nome: ");
        gets(read);
        if(pesquisar(agenda, read)==NULL){
            printf("Erro: %s não encontrado. \n", read);
            return agenda;
        }   
        nome = read;
    }
    if(agenda==NULL){
        return NULL;
    }
    if(strcmp(contato->nome, nome)==0){
        if(contato->prox!=NULL){
            agenda = contato->prox;
        }else{
            agenda = NULL;
        }
        free(contato);
        return agenda;
    }
    while(contato->prox != NULL){
        if(strcmp(contato->prox->nome, nome)==0){
            Contato * find = contato->prox;
            if(contato->prox->prox!=NULL){
                contato->prox = contato->prox->prox;
            }
            else{
                contato->prox = NULL;
            }
            free(find);
            return agenda;
        }
        contato = contato->prox;
    }
    return agenda;
}

Contato * pesquisar(Contato * agenda, char * name){
    Contato * last = agenda;
    char read[100];
    if(name==NULL){
        printf("Pesquisa\n1. Nome: ");
        gets(read);
        name = read;
    }
    if(last==NULL){
        return NULL;
    }
    while(last->prox!=NULL){
        if(strcmp(last->nome, name)==0){
            return last;
        }
        last = last->prox;
    }
    if(strcmp(last->nome, name)==0){
        return last;
    }
    return NULL;
}

int main(){
    Contato * find = NULL;
    Contato * agenda = NULL;
    agenda = load(agenda);
    int op;
    do{
        printf("\nAgenda de contatos\n1. Listar\n2. Novo contato\n3. Procurar contato\n4. Excluir contato\n-> ");
        scanf("%d", &op);
        getchar();
        system("clear");
        switch(op){
            case 1:
                lista(agenda);
                break;
            case 2:
                agenda = incluir(agenda, NULL);
                break;
            case 3:
                find = pesquisar(agenda, NULL);
                if(find==NULL){printf("Não encontrado. \n");}else{
                    printf("\nEncontrado %s:\n1. Telefone: %d\n2. Celular: %d\n\n", find->nome, find->telefone, find->celular); 
                }
                break;
            case 4:
                agenda = excluir(agenda, NULL); 
                break;
            default:
                save(agenda);
                return 0;
        }
    }while(op<5);
    return 0;
}
