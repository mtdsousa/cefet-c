/*

Considere que um banco armazena em arquivo as seguintes informações
de seus clientes: número da conta (int), nome do cliente (string), limite de
crédito (float), saldo atual (float). Este banco deseja implementar um programa
para atendimento eletrônico que permita aos clientes executarem as seguintes
operações: abertura de conta, listagem de saldo (onde o usuário entra com o
número da conta e o programa exibe o saldo), depósito (dado o número da
conta) e saque (dado o número da conta). Implemente um programa que realiza
estas operações. Você deve utilizar estruturas e arquivos binários para
armazenar as informações.

*/

#include <stdio.h>
#include <stdlib.h>
#define DATA "files/data.dat"

struct CLIENTE{
    int conta;
    char nome[100];
    float limite;
    float saldo;
};
typedef struct CLIENTE Cliente;

struct BANCO{
    Cliente clientes[100];
    int nClientes;
};
typedef struct BANCO Banco;

int existe(Banco * banco, int conta){
    int i;
    for(i=0; i<(banco->nClientes); i++){
        if(banco->clientes[i].conta == conta){
            return 1;
        }
    }
    return 0;
}

int getId(Banco * banco, int conta){
    int i;
    for(i=0; i<banco->nClientes; i++){
        if(banco->clientes[i].conta==conta){
            return i;
        }
    }
    return -1;
}

int load(Banco * banco){
    FILE * data = fopen(DATA, "rb");
    if(data==NULL){
        printf("\nErro: Não foi possível operar arquivos.\n");
        return 0;
    }
    if (fread(banco, sizeof(Banco), 1, data)!=1){
        printf("\nErro: Não foi possível operar arquivos.\n");
        return 0;
    }
    fclose(data);
    return 1;
}

int save(Banco * banco){
    FILE * data = fopen(DATA, "wb");
    if(data==NULL){
        printf("\nErro: Não foi possível operar arquivos.\n");
        return 0;    
    }
    fwrite(banco, sizeof(Banco), 1, data);
    fclose(data);
    return 1; 
}

int new(Banco * banco){
    int conta;
    printf("\nNova Conta\nEntre:\n1. Conta: ");
    scanf("%d", &conta);
    if (existe(banco, conta)){
        printf("Erro: Conta já existente. \n");
        return 0;
    }
    banco->clientes[banco->nClientes].conta = conta;
    getchar();
    printf("2. Nome: ");
    gets(banco->clientes[banco->nClientes].nome);
    printf("3. Limite: ");
    scanf("%f", &banco->clientes[banco->nClientes].limite);
    printf("4. Saldo inicial: ");
    scanf("%f", &banco->clientes[banco->nClientes].saldo);
    banco->nClientes++;
    return 1;
}

int saldo(Banco * banco){
    int conta;
    printf("\nConsulta de saldo\nEntre:\n1. Conta: ");
    scanf("%d", &conta);
    if(!existe(banco, conta)){
        printf("Erro: Conta não identificada. \n");
        return 0;
    }
    printf("Saldo: R$ %.2f\n", banco->clientes[getId(banco, conta)].saldo);
    return 1; 
}

int deposito(Banco * banco){
    int conta;
    float valor;
    printf("\nDepósito\nEntre:\n1. Conta: ");
    scanf("%d", &conta);
    if(!existe(banco, conta)){
        printf("Erro: Conta não identificada. \n");
        return 0;
    }
    printf("Valor da operação: R$ ");
    scanf("%f", &valor);
    if(valor>=0){
        banco->clientes[getId(banco, conta)].saldo += valor;
    }else{
        printf("Erro: Depósito negativo.\n");
        return 0;
    } 
    return 1;
}

int saque(Banco * banco){
    int conta;
    float valor;
    printf("\nSaque\nEntre:\n1. Conta: ");
    scanf("%d", &conta);
    if(!existe(banco, conta)){
        printf("Erro: Conta não identificada. \n");
        return 0;
    }
    printf("Valor da operação: R$: ");
    scanf("%f", &valor);
    if (banco->clientes[getId(banco, conta)].saldo>= abs(valor)){
        banco->clientes[getId(banco, conta)].saldo -= abs(valor);    
        return 1;
    }else{
        printf("Erro: Saldo insuficiente. \n");
        return 0;  
    };
}

int main(){
    Banco banco;
    banco.nClientes = 0;
    if(!load(&banco)){
        printf("Redirecionamento de operação: nova conta");
        if(new(&banco)){
            save(&banco);
            load(&banco);    
        }else{
            printf("Operação Cancelada. \n");
            return 0;    
        }
    }
    int op;
    do{
        printf("\nBanco - Escolha:\n1. Abertura de conta\n2. Listagem de saldo\n3. Depósito\n4. Saque\n5. Sair\n-> ");
        scanf("%d", &op);
        system("clear");
        switch(op){
            case 1:
                if(new(&banco)){save(&banco);}else{printf("Operação cancelada.\n");}
                break;
            case 2:
                if(!saldo(&banco)){printf("Operação cancelada. \n");}
                break;
            case 3:
                if(deposito(&banco)){save(&banco);}else{printf("Operação cancelada. \n");}
                break;
            case 4:
                if(saque(&banco)){save(&banco);}else{printf("Operação cancelada. \n");}
                break;
            default:
                op = 5;
        }
    }while(op<5);
}
