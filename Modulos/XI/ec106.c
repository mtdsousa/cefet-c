/*

Declare uma estrutura (struct) em C para armazenar informações de um
produto de uma loja. Defina corretamente o tipo de cada campo da estrutura.
Nesta estrutura deve-se armazenar as seguintes informações: nome do produto,
preço e peso. O programa deve solicitar ao usuário que digite os valores para os
campos e, em seguida, exibi-los na tela. Crie um vetor que armazena as 
informações de 10 produtos.

*/

#include <stdio.h>

struct PRODUTO{
    char nome[100];
    float preco;
    float peso;
};
typedef struct PRODUTO Produto;

int main(){
    Produto p[10];
    int i;
    for(i=0; i<10; i++){
        printf("Entre:\n1. Nome: ");
        gets(p[i].nome);
        printf("2. Preço: ");
        scanf("%f", &p[i].preco);
        printf("3. Peso: ");
        scanf("%f", &p[i].peso);
        getchar();
        putchar('\n');
    }
    printf("Resultado:");
    for (i=0; i<10; i++){
        printf("\n1. Nome: %s\n2. Preço: %.2f\n3. Peso: %.2f\n", p[i].nome, p[i].preco, p[i].peso);
    }
    return 0;    
}
