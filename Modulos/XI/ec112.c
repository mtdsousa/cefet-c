/*

Considere uma estrutura de lista onde cada elemento armazena um valor
inteiro ("int info"), e dois ponteiros, um ponteiro para o próximo elemento da
lista ("struct lista * prox") e um para o anterior ("struct lista * anterior").
Implemente as seguintes funções:

1. Lista * insere (Lista * L, int n) que insere um elemento n na última posição da
lista L e retorna um ponteiro para o primeiro elemento da lista.
2. Lista * excluir (Lista * L, int n) que remove um elemento n da lista L e retorna
um ponteiro para o primeiro elemento da lista.
3. Lista * existe (Lista * L, int n) que retorna ponteiro para o elemento que se
deseja "n" da lista. Caso o elemento não exista, retorna NULL.

*/

#include <stdio.h>
#include <stdlib.h>

struct LISTA{
    int info;
    struct LISTA * prox;
    struct LISTA * anterior;
};
typedef struct LISTA Lista;

Lista * insere(Lista * L, int n){
    Lista * new = (Lista*)malloc(sizeof(Lista));
    Lista * last = L;
    (*new).info = n;
    (*new).prox = (*new).anterior = NULL;
    if(last==NULL){
        L = new;
        return L;
    }
    while((*last).prox!= NULL){
        last = (*last).prox;
    }
    (*last).prox = new;
    (*new).anterior = last;
    (*new).prox = NULL;
    return L;
}

Lista * excluir(Lista * L, int n){
    Lista * last = L;
    Lista * aux = NULL;
    if(last==NULL){
        return NULL;
    }
    if(last->info==n){
        L = (*last).prox;
        free(last);
    }
    while((*last).prox!=NULL && (*(*last).prox).info!=n){
        last = (*last).prox;
    }
    if(((*last).prox!=NULL) && (*(*last).prox).info == n){
        if((*(*last).prox).prox == NULL){
            free((*last).prox);
            (*last).prox = NULL;
            return L;
        }
        aux = (*last).prox;
        (*last).prox = (*(*last).prox).prox;
        free(aux);
        return L;
    }
    return L;
}

Lista * existe(Lista * L, int n){
    Lista * last = L;
    if(last==NULL){
        return NULL;
    }
    while((*last).prox!=NULL){
        if((*last).info == n){
            return last;
        }
        last = (*last).prox;
    }
    if((*last).info == n){
        return last;
    }
    return NULL;
}

void listar(Lista *L){
    Lista * last = L;
    if(last==NULL){
        printf("Lista vazia.\n");
        return;
    }
    while((*last).prox!=NULL){
        printf("%d ", (*last).info);
        last = (*last).prox;
    }
    printf("%d ", (*last).info);
    putc('\n', stdout);
}

int main(){
    Lista * L = NULL;
    listar(L);
    L = insere(L, 1);
    L = insere(L, 2);
    L = insere(L, 3);
    listar(L);
    L = excluir(L, 1);
    listar(L);
    L = excluir(L, 3);
    listar(L);
    printf("%d\n", (*existe(L, 2)).info);
    printf("%p\n", existe(L, 3));
    return 0;
}
