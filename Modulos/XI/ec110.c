/*

Considere uma estrutura de listas encadeadas que armazenam valores inteiros. 
O tipo que representa um nó da lista é dado por:

struct lista{
    int info;
    struct lista * prox;
};

Implemente uma função que receba um vetor de valores inteiros com n elementos e
construa uma lista encadeada armazenando os elementos do vetor nos nós da lista.
Assim, se for recebido o vetor v[5] = {3, 8, 1, 7, 2}, a função deve retornar
uma nova lista cujo primeiro nó tem a informação 3, o segundo a informação 8, e
assim por diante. Se o vetor tiver zero elementos, a função deve ter como valor de
retorno uma lista vazia. O protótipo da função é dado por:

Lista * constroi(int n, int * v);

Implemente o programa principal para testar sua função. A lista pode ser
implementada como variável global.

*/

#include <stdio.h>
#include <stdlib.h>
#define N 5

struct LISTA{
    int info;
    struct LISTA * prox;
};
typedef struct LISTA Lista;

Lista * LISTA = NULL;

void print(Lista * lista){
    if(lista == NULL){
        printf("NULL\n");
        return;
    }
    while(1){
        printf("%d ", lista->info);
        if(lista->prox==NULL){
            break;
        }
        lista = lista->prox;
    }
    putchar('\n');
}

Lista * insere(Lista * lista, int n){
    Lista * new = (Lista *)malloc(1*sizeof(Lista));
    Lista * last = lista;
    new->info = n;
    new->prox = NULL;
    if(lista == NULL){
        lista = new;
        return lista;
    }
    while(1){
        if(last->prox == NULL){
            break;
        }
        last = last->prox;
    }
    last->prox = new;
    return lista;
}

Lista * constroi(int n, int * v){
    int i;
    for(i=0; i<n; i++){
        LISTA = insere(LISTA, v[i]);
    }
    return LISTA;
}

int main(){
    print(LISTA);
    int v[] = {3, 8, 1, 7, 2};
    constroi(N, v);
    print(LISTA); 
    return 0;
}


