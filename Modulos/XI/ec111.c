/*

Considere uma estrutura de listas encadeadas que armazenam valores inteiros. 
O tipo que representa um nó da lista é dado por:

struct lista{
    int info;
    struct lista * prox;
};

Implemente uma função que receba um vetor de valores inteiros com n elementos e
construa uma lista encadeada armazenando os elementos do vetor nos nós da lista.
Os elementos deverão ser ordenados (ordem crescente) a medida que forem sendo 
inseridos na lista.

*/

#include <stdio.h>
#include <stdlib.h>
#define N 5

struct LISTA{
    int info;
    struct LISTA * prox;
};
typedef struct LISTA Lista;

Lista * LISTA = NULL;

void print(Lista * lista){
    if(lista == NULL){
        printf("NULL\n");
        return;
    }
    while(1){
        printf("%d ", lista->info);
        if(lista->prox==NULL){
            break;
        }
        lista = lista->prox;
    };
    putchar('\n');
}

Lista * insere(Lista * lista, int n){
    Lista * new = (Lista *)malloc(1*sizeof(Lista));
    Lista * last = lista;
    new->info = n;
    new->prox = NULL;
    if(lista == NULL){
        lista = new;
        return lista;
    }
    if(new->info<=lista->info){
        new->prox = lista;
        lista = new;
    }else{
        while(new->info>last->info&&last->prox!=NULL){
            last = last->prox;
        }
        new->prox = last->prox;
        last->prox = new;
    }
    return lista;
}

Lista * constroi(int n, int * v){
    int i;
    for(i=0; i<n; i++){
        LISTA = insere(LISTA, v[i]);
    }
    return LISTA;
}

int main(){
    print(LISTA);
    int v[] = {3, 8, 1, 7, 2};
    constroi(N, v);
    print(LISTA);
    return 0;
}
