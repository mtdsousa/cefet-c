#include <stdio.h>
#include <math.h>

int Cubo(int x);

int main(){
	printf("%d\n", Cubo(1));
	printf("%d\n", Cubo(2));
	printf("%d\n", Cubo(3));
	return 0;
}

int Cubo(int x){
	return pow(x, 3);
}
