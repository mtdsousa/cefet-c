#include <stdio.h>
#include <stdlib.h>

int Minus(int valor);

int main(){
	printf("%d\n", Minus(10));
	printf("%d\n", Minus(-10));
	return 0;
}

int Minus(int valor){
	if (valor > 0)
		valor *= -1;
	return valor;
}
