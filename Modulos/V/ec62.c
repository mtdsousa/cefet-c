/*

Faça um programa para calculcar a soma dos N primeiros números
primos, sendo N definido pelo usuário na função principal "main()". O programa
deverá ter as funções "Soma_Primos" e "Primo", sendo que a primeiro será
responsável pela soma dos números que forem primos e a segundo será
responsável por verificar se o número em questão é primo ou não

*/

#include <stdio.h>

int Primo(int primo);
int Soma_Primos(int primo);
int soma_primos = 0;

int main(){
	int n, i = 2;
	int nPrimos = 0;
	printf("N: ");
	scanf("%d", &n);
	while(nPrimos < n){
		if (Primo(i)){
			nPrimos++;
		}
		i++;
	}
	printf("Resultado:\n1. Soma: %d\n\n", getSomaPrimos());
}


int Soma_Primos(int primo){
	soma_primos  += primo;
}

int getSomaPrimos(){
	return soma_primos;
}

int Primo(int primo){
	int i;
	if ((primo%2==0 && primo != 2) || primo<=1)
		return 0;
	for (i=2; i < primo; i++){
		if (primo%i==0)
			return 0;
	}
	Soma_Primos(primo);
	return 1;
}


