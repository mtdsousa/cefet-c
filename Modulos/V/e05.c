#include <stdio.h>
#include <math.h>

float VAL(float x, int n, float t){
	int i;
	float val = 0;
	for (i = 1; i <= n; i++){
		val += x/pow((1+t),i);
	}
	return val;
}

int main(){
	printf("%f\n\n", VAL(1, 1, 1));
	return 0;
}

