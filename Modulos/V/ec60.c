/*

Considere a seguinte definição de ano bissexto (ano em que o mês de
fevereiro tem 29 dias).
Um ano não divisível por 100 e divisível por 4 é bissexto;
Um ano divisível por 100 e divisível por 400 é bissexto;
Os demais anos não são bissextos.
Escreva uma função que recebe como parâmetros 3 números inteiros
correspondendo aos valores de dia, mês e ano de uma data e retorna o número
de dias já transcorridos neste ano. Teste o programa para diversas datas

*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
	if (argc != 4){
		printf("\nParâmetros não encontrados\n");
		return 0;
	}
	printf("%d\n", dias(atoi(argv[1]), atoi(argv[2]), atoi(argv[3])));
}

int dias(int dia, int mes, int ano){
	int bissexto = 0;
	int dias = 0;
	if ((ano%100!=0 && ano%4==0) || (ano%100==0 && ano%400==0))
		bissexto = 1;

	mes --;
	if (mes == 0){
		dias = dia;
	}else{
		switch (mes){
			case 12:
				dias += 31;
			case 11:
				dias += 30;
			case 10:
				dias += 31;
			case 9:
				dias += 30;
			case 8:
				dias += 31;
			case 7:
				dias += 31;
			case 6:
				dias += 30;
			case 5:
				dias += 31;
			case 4:
				dias += 30;
			case 3:
				dias += 31;
			case 2:
				if (bissexto)
					dias += 29;
				else
					dias += 28;
			case 1:
				dias += 31;
		}
		dias += dia;
	}
	return dias;
}
