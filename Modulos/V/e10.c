#include <stdio.h>

int Entre(int x, int lim_inf, int lim_sup);

int main(){
	printf("%d\n", Entre(2, 1, 5));
	printf("%d\n", Entre(5, 1, 5));
	printf("%d\n", Entre(1, 1, 5));
	printf("%d\n", Entre(0, 1, 5));
	printf("%d\n", Entre(6, 1, 5));
	return 0;
}

int Entre(int x, int lim_inf, int lim_sup){
	int afirmacao = 0;
	if (x>=lim_inf && x<=lim_sup)
		afirmacao++;
	return afirmacao;
}
