#include <stdio.h>
int Impar(int x);

int main(){
	printf("%d\n", Impar(1));
	printf("%d\n", Impar(2));
	printf("%d\n", Impar(3));
	printf("%d\n", Impar(4));
	printf("%d\n", Impar(5));
}

int Impar(int x){
	int verdade = 1;
	if (x % 2 == 0)
		verdade --;
	return verdade;
}
