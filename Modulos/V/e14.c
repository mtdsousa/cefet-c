#include <stdio.h>
#include <math.h>

int is_special(int x);

int main(){
	printf("%d\n", is_special(2));
	printf("%d\n", is_special(5));
}

int is_special(int x){
	if (x*2 == pow(x, 2))
		return 1;
	return 0;
}


