/*

Um programa para determinar o “máximo divisor comum” de dois
números inteiros A e B pode ser escrito da seguinte maneira:

enquanto B for diferente de zero
inicio
	r = resto da divisão de A por B;
	A = B;
	B = r;
fim
mdc = A;

Faça uma função em C que recebe dois valores A e B e retorna o MDC entre
eles. Implemente o programa principal para testar a função implementada

*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
	if (argc != 3){
		printf("\nParâmetros não encontrados.\n\n");
		return 0;
	}
	int a = atoi(argv[1]);
	int b = atoi(argv[2]);
	printf("%d\n", mdc(a,b));
	return 0;
}

int mdc(int a, int b){
	int r;
	while(b!=0){
		r = a%b;
		a = b;
		b = r;
	}
	return a;
}
