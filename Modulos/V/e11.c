#include <stdio.h>
//#include <ctype.h>

int isdigit(char c);
int isalpha(char c);
int isalnum(char c);
int islower(char c);
int isupper(char c);
int isspace(char c);
char tolower(char c);
char toupper(char c);

int main(){
	printf("%d\n", isdigit('0'));
	printf("%d\n", isdigit('A'));
	printf("%d\n", isalpha('A'));
	printf("%d\n", isalpha('0'));
	printf("%d\n", isalnum('Z'));
	printf("%d\n", isalnum('?'));
	printf("%d\n", islower('a'));
	printf("%d\n", islower('A'));
	printf("%d\n", isupper('A'));
	printf("%d\n", isupper('a'));
	printf("%d\n", isspace(' '));
	printf("%d\n", isspace('!'));
	printf("%c\n", tolower('A'));
	printf("%c\n", toupper('a'));
	return 0;
}

int isdigit(char c){
	int c_int = (int)c;
	if ((c_int>=48) && (c_int<=57))
		return 1;
	return 0;
}


int isalpha(char c){
	if (((int)c>=65 && (int)c<=90) || ((int)c>=97 && (int)c<=122))
		return 1;
	return 0;
}

int isalnum(char c){
	if (isdigit(c) || isalpha(c))
		return 1;
	return 0;
}

int islower(char c){
	if (isalpha(c) && c>= 97)
		return 1;
	return 0;
}

int isupper(char c){
	if (isalpha(c) && c<=90)
		return 1;
	return 0;
}

int isspace(char c){
	if (c == 9 || c == 32)
		return 1;
	return 0;
}

char tolower(char c){
	c += 32;
	return c;
}

char toupper(char c){
	c -= 32;
	return c;
}

