#include <stdio.h>

int isVogal(char ch);

int main(){
	printf("%d\n", isVogal('I'));
	printf("%d\n", isVogal('Z'));
}

int isVogal(char ch){
	switch (ch){
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			return 1;
	}
	return 0;
}
