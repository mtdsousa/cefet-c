/*

Faça um programa para identificar se um número inteiro positivo é primo.
Você deve implementar uma funcão específica que recebe um número inteiro e
retorna o valor "0" caso o número NÃO seja primo e "1" caso contrário.

*/

#include <stdio.h>

int main(){
	printf("%d\n", primof(2));
	printf("%d\n", primof(7));
	printf("%d\n", primof(12));
	return 0;
}

int primof(int primo){
	int i;

	if ((primo%2==0 && primo != 2) || primo<=1)
		return 0;

	for (i=2; i < primo; i++){
		if (primo%i==0)
			return 0;
	}
	return 1;
}
