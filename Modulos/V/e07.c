#include <stdio.h>

long int num(int n_horas, char tipo);

int main(){
	printf("%ld\n", num(3, 'h'));
	printf("%ld\n", num(3, 'm'));
	printf("%ld\n", num(3, 's'));
	return 0;
}

long int num(int n_horas, char tipo){
	long int num = n_horas;
	switch (tipo){
		case 's': num *= 60;
		case 'm': num *= 60;
	}
	return num;
}

