#include <stdio.h>
#include <math.h>

int is_square(int x, int y);

int main(){
	printf("%d\n", is_square(4, 2));
	printf("%d\n", is_square(4, 3));
	return 0;
}

int is_square(int x, int y){
	if (x == pow(y, 2))
		return 1;
	return 0;
}
