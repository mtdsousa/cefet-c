#include <stdio.h>

long int n_segundos(int n_horas);

int main(){
	printf("%ld\n", n_segundos(1));
	printf("%ld\n\n", n_segundos(2));
	return 0;
}

long int n_segundos(int n_horas){
	long int segundos = 0;
	segundos = (n_horas*60*60);
	return segundos;
}
