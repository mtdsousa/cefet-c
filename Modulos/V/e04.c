#include <stdio.h>

int Abs(int x);

int main(){
	printf("%d\n", Abs(5));
	printf("%d\n", Abs(-5));
	return 0;
}

int Abs(int x){
	if (x<=0)
		x*=(-1);
	return x;
}
