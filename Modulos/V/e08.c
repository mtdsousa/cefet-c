#include <stdio.h>

float Max(float x, float y, float w);

int main(){
	printf("%f\n", Max(1, 2, 3));
	printf("%f\n", Max(3, 2, 1));
	printf("%f\n", Max(1, 3, 2));
	printf("%f\n", Max(2, 3, 1));
	printf("%f\n", Max(2, 1, 3));
	return 0;
}

float Max(float x, float y, float w){
	float maior = x;
	if (y > maior)
		maior = y;
	if (w > maior)
		maior = w;
	return maior;
}
