/*

Faça um programa capaz de identificar se um número é par ou ímpar

*/

#include <stdio.h>
int main(){
	int a;
	printf("Entre: \n1. Número: ");
	scanf("%d", &a);
	printf("Resultado: \n a)");
	if (a%2==0){
		printf(" Número Par\n");
	}else{
		printf(" Número Ímpar\n");
	}
	return 0;
}
