/*

Faça um programa que leia 3 números e determine quantos são iguais

*/

#include <stdio.h>
int main(){
	int a, b, c;
	printf("Entre: \n1. Número 1: ");
	scanf("%d", &a);
	printf("2. Número 2: ");
	scanf("%d", &b);
	printf("3. Número 3: ");
	scanf("%d", &c);
	if (a == b){
		if (b==c){
			printf("Resultado: \na) Todas (3) entradas são iguais.\n");
		}else{
			printf("Resultado: \na) Somente duas entradas são iguais.\n");
		}
	}else{
		if (b==c){
			printf("Resultado: \na) Somente duas entradas são iguais.\n");
		}else{
			if (a==c){
				printf("Resultado: \na) Somente duas entradas são iguais.\n");
			}else{
				printf("Resultado: \na) Nenhuma das entradas são iguais.\n");
			}
		}
	}
	return 0;
}
