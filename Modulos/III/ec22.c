/*

Faça um programa capaz de identificar se um número é positivo, negativo
ou zero

*/

#include <stdio.h>
int main(){
	int a;
	printf("Entre: \n1. Número: ");
	scanf("%d", &a);
	printf("Resultado: \na) ");
	if (a>0){
		printf("Positivo\n");
	}else if (a==0){
		printf("Zero\n");
	}else{
		printf("Negativo\n");
	}
	return 0;
}
