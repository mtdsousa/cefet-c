/*

Faça um programa que leia dois números inteiros e determine qual dos
dois é maior. Considere que os dois números serão diferentes.

*/

#include <stdio.h>
int main(){
	int maior, outro;
	printf("Entre: \n1. Número 1: ");
	scanf("%d", &maior);
	printf("2. Número 2: ");
	scanf("%d", &outro);
	if (outro > maior)
		maior = outro;
	printf("Resultado: \na) Maior: %d\n", maior);
	return 0;
}
