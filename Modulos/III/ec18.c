 /*
 
Faça um programa que leia dois números inteiros e determine qual dos
dois é maior. Considere que os dois números podem ser iguais. Neste caso, o
programa deve escrever uma mensagem para o usuário informando-o de que
deve entrar com números diferentes

*/

#include <stdio.h>
int main(){
	int maior, outro;
	printf("Entre: \n1. Número 1: ");
	scanf("%d", &maior);
	printf("2. Número 2 (diferente de 1): ");
	scanf("%d", &outro);
	printf("Resultado: \n");
	if (outro == maior)
		printf("a) São iguais\n");
	else{
		if (outro > maior){
			maior = outro;
		}
		printf("a) Maior: %d\n", maior);
	}
	return 0;
}

