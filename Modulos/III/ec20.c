/*

Faça um programa capaz de identificar se um número é igual a 1, 5 ou
10. Caso não seja nenhum desses valores, retornar a mensagem “Valor
inválido”

*/

#include <stdio.h>
int main(){
	float a;
	printf("Entre: \n1. Com um número (1.5 ou 10): ");
	scanf("%f", &a);
	if ((a == 1.5)||(a == 10)){
		printf("Resposta: \na) Número (%.1f) válido.\n", a);
	}else{
		printf("Resposta: \na) Número inválido. \n");
	}
	return 0;
}
