/*

Faça um programa capaz de identificar se um número é um ano bissexto.
Considere que para o ano ser bissexto basta que seja divisível por 400. Caso
contrário, precisa ser divisível por 4 e não ser divisível por 100. Faça uma
condição composta que englobe todas as regras para a definição do ano
bissexto

*/

#include <stdio.h>
int main(){
	int ano;
	printf("Entre: \n1. Ano: ");
	scanf("%d", &ano);
	printf("Resposta: \na) ");
	if (((ano%4==0)&&(ano%100!=0))||(ano%400==0))
		printf("Ano bissexto\n");
	else
		printf("Ano não-bissexto\n");
	return 0;
}
