/*

Faça um algoritmo que simule uma calculadora com as quarto operações
básicas (+,-,*,/). O algoritmo deve solicitar ao usuário a entrada de dois
operandos e da operação a ser executada, na forma de menu. Dependendo da
opção escolhida, deve ser executada a operação solicitada e escrito seu
resultado. Utilize uma variável do tipo caractere para armazenar a operação e
utilize o comando caso para escolher a operação a partir do operador. (Solução
PROG0316.c, página 73 do livro texto)

*/

#include <stdio.h>
int main(){
	float a, b, ab;
	char operacao;
	printf("Entre:\n1. A: ");
	scanf("%f", &a);
	printf("2. Operação (+,-,*,/): ");
	scanf(" %c", &operacao);
	printf("3. B: ");
	scanf("%f", &b);
	switch(operacao){
		case '+':
			ab = a+b;	
			break;
		case '-':
			ab = a-b;
			break;
		case '*':
			ab = a*b;
			break;
		case '/':
			ab = a/b;
			break;
		default:
			printf("Operação não reconhecida. \n");
			return 0;
	}
	printf("Resultado: \na) %.1f %c %.1f = %.1f\n", a, operacao, b, ab);
	return 0;
}

