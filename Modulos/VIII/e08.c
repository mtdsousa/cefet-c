#include <stdio.h>
#include <ctype.h>
#include <strings.h>
char *StrDelUpper(char *s);

int main(){
    char s[100] = "Linguagem C";
    printf(StrDelUpper(s)); 
}

char *StrDelUpper(char *s){
    int i, j = 0;
    char copy[100];
    strcpy(copy, s);
    for(i=0; copy[i]!='\0'; i++){
        if(!isupper(copy[i])){
           s[j] = copy[i];
           j++;
        }  
    }
    s[j] = '\0';
    return s;
}
