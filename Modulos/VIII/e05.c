#include <stdio.h>

char *First_Vogal(char *s);

int main(){
    char *r = First_Vogal("mtdsousa");
    if(r!=NULL){
        puts(r);
    }
    return 0;
}

char *First_Vogal(char *s){
    int i;
    char *e;
    e = NULL;
    for(i=0; s[i]!='\0'; i++){
        if(s[i]==97 || s[i]==101 || s[i]==105 || s[i]==111 || s[i]==117){
            e = &s[i];
            break;
        }
    }
    return e;
}
