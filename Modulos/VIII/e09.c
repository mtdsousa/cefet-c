#include <stdio.h>
#include <strings.h>

char *StrDelStr(char *s1, char *s2);

int main(){
    char s[100] = "Atualmente a tua mente atua ou mente?\n";
    printf(StrDelStr(s, "mente"));
}

char *StrDelStr(char *s1, char *s2){
    int len, i, c=0;
    char r[100];
    char *e = strstr(s1, s2);
    for(len=0; s2[len]!='\0'; len++);
    for(i=0; s1[i]!='\0'; i++){
        if(e == &s1[i]){
            i+=len;
            continue;
        }
        r[c] = s1[i];
        c++;
    }
    r[c] = '\0';
    strcpy(s1, r);
    return s1;
}
