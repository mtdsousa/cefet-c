#include <stdio.h>

int main(){
    char *r = strstr("me@mtdsousa.com, mtdsousa.com", ".");
    if(*r!=NULL){
        printf("%s\n", r);
    }
    return 0;    
}

char *strstr(char *str1, char *str2){
    int i, j, test = 1;
    char *e;
    e = NULL;
    for(i=0; str1[i]!='\0'; i++){
        test = 1;
        for(j=0; str2[j]!='\0'; j++){
            if(str1[i+j]!=str2[j]){
               test = 0;
            }
        }
        if(test){
            e = &str1[i];
        }
    }
    return e;
}
