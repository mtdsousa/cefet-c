#include <stdio.h>
#include <strings.h>

char *strins(char *dest, char *orig);

int main(){
    char s[100] = "Autônoma";
    strins(s, "Universidade");
    printf(s);
    return 0;
}

char *strins(char *dest, char *orig){
    int i, j;
    char copy[100];
    strcpy(copy, dest);
    for(i=0; orig[i]!='\0'; i++){
        dest[i] = orig[i];  
    }
    for(i=0; dest[i]!='\0'; i++);
    for(j=0; copy[j]!='\0'; j++){
        dest[j+i] = copy[j];
    }
    dest[j+i] = '\0';
    return dest;
}
