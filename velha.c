#include <stdio.h>
#define D 3
#define true 1
#define false 0
#define DEBUG 0

void ui(char game[D][D]);
void transform(char game[D][D], int one[D][D], int two[D][D], char onePerson, char twoPerson);
void make(char element[D][D]);
int conflict(int i, int j, int one[D][D], int two[D][D]);
void start(int player[D][D]);
void turn(int i, int j, int player[D][D]);
void automatic(int one[D][D], int two[D][D], int prob[D][D]);
int reverse(int x, int a, int b);
int finish(int one[D][D], int two[D][D]);
int first = 1;
int soma(int player[D][D], int modo, int lc);
void printm(int m[D][D]);
int somad(int player[D][D], int modo);

int main(){
    system("clear");
    int opcao = 0;
    
    //Menu principal
    printf("*** JOGO DA VELHA ***\n\n1. Player vs PC\n2. Player vs Player\n0. Sair\n\nOpção: ");    
    scanf("%d", &opcao);
        
    //Escape
    if (!opcao || opcao>2){ return 0;}
        
    //Inicializa-se matriz do jogo
    char game[D][D];
    make(game);
        
    //Jogador 1, jogador 2 e alternativas
    int one[D][D], two[D][D], alt[D][D];
    start(one);
    start(two);
    start(alt);
        
    int player = 0;
    int pc = (opcao==1)? 1 : 0;
    int x, y;
    int times = 0;
        
    //Interface
    ui(game);
        
    while(1){
        //Verifica término de jogo
        if (finish(one, two) || times >= D*D){
            if(DEBUG){
                printm(one);
                putchar('\n');
                printm(two);
            }
            break;  
        }

        printf("Jogador %d, qual sua jogada (Linha Coluna)? ", player+1);
        scanf("%d %d", &x, &y);
            
        //Verifica se a jogada é válida
        if(!conflict(x-1, y-1, one, two)){
            //Abre turnos
            if(!player){
                turn(x, y, one);
                player = 1;
            }else{
                turn(x, y, two);
                player = 0; 
            }
            //Jogadas automáticas
            if ((pc && !finish(one, two)) && times<D*D){
                automatic(one, two, alt);
                player = 0;
                times +=1; 
            }
                
            transform(game, one, two, 'X', 'O');
            ui(game);
            times += 1; 
        }else{
            printf("Jogada inválida. Tente novamente. \n");
        }
    }
}

//Matriz game com caracteres representativos
void transform(char game[D][D], int one[D][D], int two[D][D], char playerone, char playertwo){
    int i, j;
    for(i=0; i<D; i++){
        for(j=0; j<D; j++){
            if (one[i][j]){ game[i][j] = playerone; }
            if (two[i][j]){ game[i][j] = playertwo; }
        }   
    }   
}

//Interface de jogo
void ui(char game[D][D]){
    int i, j;
    system("clear");
    printf("*** JOGO DA VELHA ***\n\n");
    for(i=0; i<D; i++){
        for(j=0; j<D; j++){
            printf("%c %c", game[i][j] == '\0'? ' ' : game[i][j], j!=2? '|' : ' ');     
        }
        if(i!=2){ printf("\n--------\n"); }
    }
    putchar('\n');
    putchar('\n');
}

//Inicia-se matriz de caracteres
void make(char m[D][D]){
    int i, j;
    for(i=0; i<D; i++){
        for(j=0; j<D; j++){
            m[i][j] = '\0';
        }
    }
}

//Inicia-se matriz de inteiros
void start(int m[D][D]){
    int i, j;
    for (i=0; i<D; i++){
        for(j=0; j<D; j++){
            m[i][j] = 0;
        }
    }
}

//Verifica conflito de jogadas
int conflict(int i, int j, int one[D][D], int two[D][D]){
    if((one[i][j]!=0) || (two[i][j]!=0)){return true;}
    return false;
}

//Marca o turno
void turn(int i, int j, int player[D][D]){
    player[i-1][j-1] = 1;
}

//Jogadas automáticas
void automatic(int one[D][D], int two[D][D], int alt[D][D]){
    int x = 0, y = 0, k = 0, l = 0, d1 = 0, d2 = 0, best = 0;
    
    //Primeira jogada
    if (first){
        int i = 0; int j = 0;
        for (i=0; i<D; i++){
            for(j=0; j<D; j++){
                if (one[i][j]){
                    //Jogada central
                    if (i==j && i==1){
                        //Extremos são alternativas
                        alt[0][0] = 1, alt[0][2] = 1, alt[2][0] = 1, alt[2][2] = 1;
                    //Jogada nos extremos
                    }else if((i==0 || i==2)&&(j==0 || j==2)){
                        //Centro mais plausível
                        alt[1][1] = 3;
                        //Reverso provável
                        alt[reverse(i, 0, 2)][reverse(j, 0, 2)] = 2;
                        //Outros extremos consideráveis
                        if (i==j){
                            alt[0][2] = 1;
                            alt[2][0] = 1;

                        }else{
                            alt[0][0] = 1;
                            alt[2][2] = 1;
                        }
                    //Demais jogadas        
                    }else{
                        //Adjacentes
                        if (i == 1){
                            alt[0][j] = 1;
                            alt[2][j] = 1;      
                        }else if(j==1){
                            alt[i][0] = 1;
                            alt[i][2] = 1;
                        }
                        //Reverso
                        alt[reverse(i, 0, 2)][reverse(j, 0, 2)] = 1;
                    }
                }   
            }
        }
        first = 0;
    }

    //Verificando oportunidade de término pelo jogador 1
    for (k=0; k<D; k++){
        for (l=0; l<D; l++){
            if (soma(two, 1, k)==2){
                if (!conflict(k, l, one, two)){turn(k+1, l+1, two);return;}
            }
            if (soma(two, 0, k)==2){
                if (!conflict(l, k, one, two)){turn(l+1, k+1, two);return;}
            }
            if ((somad(two, 1)==2)&&(k==l)){
                if (!conflict(k, l, one, two)){turn(k+1, l+1, two);return;}
            }
            if ((somad(two, 0)==2)&&(k+l==2)){
                if (!conflict(k, l, one, two)){turn(k+1, l+1, two);return;} 
            }
        }
    }
    //Jogador 2 
    for (k=0; k<D; k++){
        for (l=0; l<D; l++){
            if (soma(one, 1, k)==2){
                if (!conflict(k, l, one, two)){turn(k+1, l+1, two);return;}
            }
            if (soma(one, 0, k)==2){
                if (!conflict(l, k, one, two)){turn(l+1, k+1, two);return;}
            }
            if ((somad(one, 1)==2)&&(k==l)){
                if (!conflict(k, l, one, two)){turn(k+1, l+1, two);return;}
            }
            if ((somad(one, 0)==2)&&(k+l==2)){
                if (!conflict(k, l, one, two)){turn(k+1, l+1, two);return;} 
            }
        }
    }

    //Procurando melhor alternativa
    for (k =0; k<D; k++){
        for (l= 0; l<D; l++){
            if (alt[k][l]>best){
                if(!conflict(k, l, one, two)){
                    best = alt[k][l];
                    x = k, y = l;
                }
            }
        }
    }
    if (alt[x][y] != 0){
        alt[x][y] = 0;  
        x++, y++;
        turn(x, y, two);
        return;
    }else{
        for (k=0; k<D; k++){
            for(l=0; l<D; l++){
                if (!conflict(k, l, one, two)){
                    turn(k+1, l+1, two);
                    return;
                }
            }
        }
    }
        
}

//Escolhe o inverso de x entre a e b
int reverse(int x, int a, int b){
    if (x == a){return b;}else if(x==b){return a;}else{return x;}
}

//Soma coluna(0)/linha(1)
int soma(int player[D][D], int modo, int lc){
    int i, soma=0;
    for(i=0; i<D; i++){
        soma += player[modo? lc : i][modo? i : lc];
    }
    return soma;
}

//Soma diagonais (1)/(0)
int somad(int player[D][D], int modo){
    int i, j, s = 0;
    for(i=0; i<D; i++){
        for(j=0; j<D; j++){
            if(modo && i==j){
                s+=player[i][j];
            }else if(!modo && i+j==2){
                s+=player[i][j];
            }
        }
    }
    return s;
}

//Verifica término do jogo
int finish(int one[D][D], int two[D][D]){
    int i; int j;
    for(i=0; i<D; i++){
        if ((soma(one, 1, i)==3) || (soma(two, 1, i)==3)){return 1;} //Linhas
        if ((soma(one, 0, i)==3) || (soma(two, 0, i)==3)){return 1;} //Colunas
    }
    //Diagonais
    if ((somad(one, 1)== 3) || (somad(two, 1) == 3)){return 1;}
    if ((somad(one, 0)== 3) || (somad(two, 0) == 3)){return 1;}
    return 0;
}

//DEBUG: Imprime matrizes numéricas
void printm(int m[D][D]){
    int i, j;
    for(i=0; i<D; i++){
        for(j=0; j<D; j++){
            printf("%d ", m[i][j]);
        }
        putchar('\n');
    }
}
